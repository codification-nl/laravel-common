<?php

namespace Codification\Common\Test\TestCase\Phone
{
	use Codification\Common\TestSuite\TestCase;
	use Codification\Common\Phone\Phone;
	use Codification\Common\Phone\PhoneType;

	class ValidationTest extends TestCase
	{
		/**
		 * @test
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_can_validate() : void
		{
			self::assertTrue(Phone::validate('0474-12-34-56', 'be', PhoneType::MOBILE()));
			self::assertNotTrue(Phone::validate('0474-12-34-56', 'be', PhoneType::FIXED()));
			self::assertTrue(Phone::validate('0474-12-34-56', 'be', PhoneType::BOTH()));

			self::assertNotTrue(Phone::validate('0474-12-34-56', 'nl', PhoneType::MOBILE()));
			self::assertNotTrue(Phone::validate('0474-12-34-56', 'nl', PhoneType::FIXED()));
			self::assertNotTrue(Phone::validate('0474-12-34-56', 'nl', PhoneType::BOTH()));
		}
	}
}