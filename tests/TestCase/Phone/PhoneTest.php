<?php

namespace Codification\Common\Test\TestCase\Phone
{
	use Codification\Common\TestSuite\TestCase;
	use Codification\Common\Phone\ParseErrorType;
	use Codification\Common\Phone\Phone;

	class PhoneTest extends TestCase
	{
		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \PHPUnit\Framework\Exception
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_has_helper_function() : void
		{
			$object = \phone('0612345678', 'nl');
			self::assertInstanceOf(Phone::class, $object);
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_handle_null() : void
		{
			$parse_error = ParseErrorType::NONE();
			$object      = Phone::make(null, 'nl', $parse_error);
			self::assertNull($object);
			self::assertNotNull($parse_error);
			self::assertTrue($parse_error->eq(ParseErrorType::NOT_A_NUMBER()));
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_throws_on_invalid_country() : void
		{
			$parse_error = ParseErrorType::NONE();
			$object      = Phone::make('0474-12-34-56', 'abc', $parse_error);
			self::assertNull($object);
			self::assertNotNull($parse_error);
			self::assertTrue($parse_error->eq(ParseErrorType::INVALID_COUNTRY_CODE()));
		}

		/**
		 * @test
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_can_format() : void
		{
			$parse_error = ParseErrorType::NONE();
			$object      = Phone::make('0474-12-34-56', 'be', $parse_error);
			self::assertNotNull($object);
			self::assertEquals('0032474123456', $object->format('nl'));
			self::assertNotNull($parse_error);
			self::assertTrue($parse_error->eq(ParseErrorType::NONE()));
		}

		/**
		 * @test
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_can_format_with_null() : void
		{
			$this->app->setLocale('nl');

			$parse_error = ParseErrorType::NONE();

			$object = Phone::make('0474-12-34-56', 'be', $parse_error);
			self::assertNotNull($object);
			self::assertEquals('0032474123456', $object->format());
			self::assertNotNull($parse_error);
			self::assertTrue($parse_error->eq(ParseErrorType::NONE()));
		}
	}
}