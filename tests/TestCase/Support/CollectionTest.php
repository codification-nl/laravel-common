<?php

namespace Codification\Common\Test\TestCase\Support
{
	use Codification\Common\Support\CollectionUtils;
	use Codification\Common\TestSuite\TestCase;
	use Illuminate\Support\Collection;

	class CollectionTest extends TestCase
	{
		/**
		 * @test
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 */
		public function it_can_create_paginator() : void
		{
			static::assertTrue(Collection::hasMacro('paginate'));

			$per_page = CollectionUtils::PER_PAGE;
			$page     = 1;
			$data     = \explode('_', __METHOD__);

			$object = new Collection($data);
			$utils  = new CollectionUtils();

			/** @var \Illuminate\Pagination\LengthAwarePaginator $paginator */
			$paginator = $utils->paginate()->call($object, $per_page, ['*'], 'page', $page);

			static::assertTrue($paginator->onFirstPage());

			static::assertSame($page, $paginator->currentPage());

			static::assertSame(\count($data), $paginator->count());
			static::assertSame(\count($data), $paginator->total());

			static::assertSame($object->count(), $paginator->total());
			static::assertSame($per_page, $paginator->perPage());

			static::assertSame($object->count(), $paginator->count());
			static::assertSame($object->all(), $paginator->all());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 */
		public function it_can_have_pages() : void
		{
			$per_page = 1;
			$page     = 1;
			$data     = \explode('_', __METHOD__);

			$object = new Collection($data);
			$utils  = new CollectionUtils();

			/** @var \Illuminate\Pagination\LengthAwarePaginator $paginator */
			$paginator = $utils->paginate()->call($object, $per_page, ['*'], 'page', $page);

			static::assertTrue($paginator->onFirstPage());

			static::assertSame($page, $paginator->currentPage());

			static::assertNotSame(\count($data), $paginator->count());
			static::assertSame(\count($data), $paginator->total());

			static::assertSame($object->count(), $paginator->total());
			static::assertSame($per_page, $paginator->perPage());

			static::assertNotSame($object->count(), $paginator->count());
			static::assertNotSame($object->all(), $paginator->all());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 */
		public function it_can_paginate() : void
		{
			$per_page = 1;
			$page     = 2;
			$data     = \explode('_', __METHOD__);

			$object = new Collection($data);
			$utils  = new CollectionUtils();

			/** @var \Illuminate\Pagination\LengthAwarePaginator $paginator */
			$paginator = $utils->paginate()->call($object, $per_page, ['*'], 'page', $page);

			static::assertNotTrue($paginator->onFirstPage());

			static::assertSame($page, $paginator->currentPage());

			static::assertNotSame(\count($data), $paginator->count());
			static::assertSame(\count($data), $paginator->total());

			static::assertSame($object->count(), $paginator->total());
			static::assertSame($per_page, $paginator->perPage());

			static::assertNotSame($object->count(), $paginator->count());
			static::assertNotSame($object->all(), $paginator->all());
		}
	}
}