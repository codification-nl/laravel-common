<?php

namespace Codification\Common\Test\TestCase\Validation
{
	use Codification\Common\TestSuite\TestCase;
	use Codification\Common\Validation\Rule;
	use Illuminate\Support\Facades\Validator;

	class PhoneRuleTest extends TestCase
	{
		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 */
		public function it_can_pass_validation() : void
		{
			$test = [
				'test'         => '0474-12-34-56',
				'test_country' => 'be',
			];

			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'a' => $test,
				'b' => $test,
			], [
				'a.test' => Rule::phone(),
				'b.test' => ['phone'],
			]);

			self::assertTrue($validator->passes());
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 */
		public function it_can_fail_validation() : void
		{
			$test = [
				'test'         => '0474-12-34-56',
				'test_country' => 'nl',
			];

			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'a' => $test,
				'b' => $test,
			], [
				'a.test' => Rule::phone(),
				'b.test' => ['phone'],
			]);

			self::assertTrue($validator->fails());
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 */
		public function it_can_fail_country_validation() : void
		{
			$data = [
				'test' => '0474-12-34-56',
			];

			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'a' => $data,
				'b' => $data,
			], [
				'a.test' => Rule::phone(),
				'b.test' => ['phone'],
			]);

			self::assertTrue($validator->fails());
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 */
		public function it_can_fail_fixed_validation() : void
		{
			$data = [
				'test'         => '0474-12-34-56',
				'test_country' => 'be',
			];

			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'a' => $data,
				'b' => $data,
			], [
				'a.test' => Rule::phone()->fixed(),
				'b.test' => ['phone:,fixed'],
			]);

			self::assertTrue($validator->fails());
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 */
		public function it_can_pass_mobile_validation() : void
		{
			$data = [
				'test'         => '0474-12-34-56',
				'test_country' => 'be',
			];

			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'a' => $data,
				'b' => $data,
			], [
				'a.test' => Rule::phone()->mobile(),
				'b.test' => ['phone:,mobile'],
			]);

			self::assertTrue($validator->passes());
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 */
		public function it_can_pass_custom_validation() : void
		{
			$data = [
				'test'    => '0474-12-34-56',
				'country' => 'be',
			];

			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'a' => $data,
				'b' => $data,
			], [
				'a.test' => Rule::phone('a.country'),
				'b.test' => ['phone:b.country'],
			]);

			self::assertTrue($validator->passes());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 */
		public function it_throws_on_invalid_type() : void
		{
			$this->expectException(\Codification\Common\Enum\Exceptions\ParseException::class);

			$data = [
				'test'         => '0474-12-34-56',
				'test_country' => 'be',
			];

			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'a' => $data,
				'b' => $data,
			], [
				'a.test' => Rule::phone()->type('invalid'),
				'b.test' => ['phone:,invalid'],
			]);

			self::assertTrue($validator->passes());
		}
	}
}