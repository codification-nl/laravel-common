<?php

namespace Codification\Common\Test\TestCase\Validation
{
	use Codification\Common\TestSuite\TestCase;
	use Illuminate\Support\Facades\Validator;

	class EnumRuleTest extends TestCase
	{
		/**
		 * @test
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_can_pass_validation() : void
		{
			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'enum' => 'foo',
			], [
				'enum' => TestEnum::rule(),
			]);

			self::assertTrue($validator->passes());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_can_fail_validation() : void
		{
			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'enum' => 'bar',
			], [
				'enum' => TestEnum::rule(),
			]);

			self::assertTrue($validator->fails());
		}
	}
}