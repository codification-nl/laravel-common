<?php

namespace Codification\Common\Test\TestCase\Validation
{
	use Carbon\CarbonPeriod;
	use Codification\Common\TestSuite\TestCase;
	use Codification\Common\Validation\Rule;
	use Illuminate\Support\Facades\Validator;

	class PeriodRuleTest extends TestCase
	{
		/**
		 * @test
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_can_pass_validation() : void
		{
			$data = [
				'test' => CarbonPeriod::day()->toIso8601String(),
			];

			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'a' => $data,
				'b' => $data,
			], [
				'a.test' => Rule::period(),
				'b.test' => ['period'],
			]);

			self::assertTrue($validator->passes());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_can_fail_validation() : void
		{
			$data = [
				'test' => 'invalid',
			];

			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'a' => $data,
				'b' => $data,
			], [
				'a.test' => Rule::period(),
				'b.test' => ['period'],
			]);

			self::assertTrue($validator->fails());
		}
	}
}