<?php

namespace Codification\Common\Test\TestCase\Validation
{
	use Codification\Common\TestSuite\TestCase;
	use Codification\Common\Validation\Rule;
	use Illuminate\Support\Facades\Validator;

	class AlphaSpacesRuleTest extends TestCase
	{
		/**
		 * @test
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_can_pass_validation() : void
		{
			$data = [
				'test' => 'Hello-World',
			];

			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'a' => $data,
				'b' => $data,
			], [
				'a.test' => Rule::alpha_spaces(),
				'b.test' => ['alpha_spaces'],
			]);

			self::assertTrue($validator->passes());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_can_fail_validation() : void
		{
			$data = [
				'test' => '1_2_3_4',
			];

			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'a' => $data,
				'b' => $data,
			], [
				'a.test' => Rule::alpha_spaces(),
				'b.test' => ['alpha_spaces'],
			]);

			self::assertTrue($validator->fails());
		}
	}
}