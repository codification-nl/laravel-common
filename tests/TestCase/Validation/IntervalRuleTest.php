<?php

namespace Codification\Common\Test\TestCase\Validation
{
	use Carbon\CarbonInterval;
	use Codification\Common\TestSuite\TestCase;
	use Codification\Common\Validation\Rule;
	use Illuminate\Support\Facades\Validator;

	class IntervalRuleTest extends TestCase
	{
		/**
		 * @test
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_can_pass_validation() : void
		{
			$data = [
				'test' => CarbonInterval::hour()->spec(),
			];

			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'a' => $data,
				'b' => $data,
			], [
				'a.test' => Rule::interval(),
				'b.test' => ['interval'],
			]);

			self::assertTrue($validator->passes());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_can_fail_validation() : void
		{
			$data = [
				'test' => 'invalid',
			];

			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'a' => $data,
				'b' => $data,
			], [
				'a.test' => Rule::interval(),
				'b.test' => ['interval'],
			]);

			self::assertTrue($validator->fails());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_allows_empty_validation() : void
		{
			$data = [
				'test' => CarbonInterval::seconds(0)->spec(),
			];

			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'a' => $data,
				'b' => $data,
			], [
				'a.test' => Rule::interval(true),
				'b.test' => ['interval:true'],
			]);

			self::assertTrue($validator->passes());
		}
	}
}