<?php

namespace Codification\Common\Test\TestCase\Validation
{
	use Codification\Common\Enum;

	/**
	 * @method static TestEnum FOO()
	 *
	 * @template-extends \Codification\Common\Enum\Enum<string>
	 */
	class TestEnum extends Enum\Enum
	{
		/** @var string */
		public const FOO = 'foo';
	}
}