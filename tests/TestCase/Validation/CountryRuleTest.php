<?php

namespace Codification\Common\Test\TestCase\Validation
{
	use Codification\Common\TestSuite\TestCase;
	use Codification\Common\Validation\Rule;
	use Illuminate\Support\Facades\Validator;

	class CountryRuleTest extends TestCase
	{
		/**
		 * @test
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_can_pass_validation() : void
		{
			$data = [
				'test' => 'nl',
			];

			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'a' => $data,
				'b' => $data,
			], [
				'a.test' => Rule::country(),
				'b.test' => ['country'],
			]);

			self::assertTrue($validator->passes());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_can_fail_validation() : void
		{
			$data = [
				'test' => 'abc',
			];

			/** @var \Illuminate\Validation\Validator $validator */
			$validator = Validator::make([
				'a' => $data,
				'b' => $data,
			], [
				'a.test' => Rule::country(),
				'b.test' => ['country'],
			]);

			self::assertTrue($validator->fails());
		}
	}
}