<?php

namespace Codification\Common\Test\TestCase\Enum
{
	use Codification\Common\TestSuite\TestCase;

	class ValidationTest extends TestCase
	{
		/**
		 * @test
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_validate() : void
		{
			self::assertTrue(TestEnum::isValid('hello'));
			self::assertNotTrue(TestEnum::isValid('bye'));
		}
	}
}