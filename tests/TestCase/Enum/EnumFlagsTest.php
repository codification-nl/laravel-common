<?php

namespace Codification\Common\Test\TestCase\Enum
{
	use Codification\Common\TestSuite\TestCase;

	class EnumFlagsTest extends TestCase
	{
		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_have_flags() : void
		{
			$object = TestEnumHasFlags::FIRST();
			self::assertTrue($object->eq(TestEnumHasFlags::FIRST()));
			self::assertNotTrue($object->has(TestEnumHasFlags::SECOND()));
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_set_flags() : void
		{
			$object = TestEnumHasFlags::NONE();

			$object = $object->set(TestEnumHasFlags::FIRST());
			self::assertTrue($object->has(TestEnumHasFlags::FIRST()));
			self::assertNotTrue($object->has(TestEnumHasFlags::SECOND()));

			$object = $object->set(TestEnumHasFlags::SECOND());
			self::assertTrue($object->has(TestEnumHasFlags::FIRST()));
			self::assertTrue($object->has(TestEnumHasFlags::SECOND()));
			self::assertTrue($object->eq(TestEnumHasFlags::BOTH()));
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_remove_flags() : void
		{
			$object = TestEnumHasFlags::BOTH();
			$object = $object->remove(TestEnumHasFlags::SECOND(), TestEnumHasFlags::FIRST());
			self::assertNotTrue($object->has(TestEnumHasFlags::FIRST()));
			self::assertNotTrue($object->has(TestEnumHasFlags::SECOND()));
			self::assertTrue($object->eq(TestEnumHasFlags::NONE()));
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_make_flags_from_string() : void
		{
			$object = TestEnumHasFlags::make('FIRST');
			self::assertTrue($object->eq(TestEnumHasFlags::FIRST()));
			self::assertNotTrue($object->has(TestEnumHasFlags::SECOND()));

			$object = TestEnumHasFlags::make('FIRST,SECOND');
			self::assertTrue($object->has(TestEnumHasFlags::FIRST()));
			self::assertTrue($object->has(TestEnumHasFlags::SECOND()));
			self::assertTrue($object->eq(TestEnumHasFlags::BOTH()));
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_make_flags_from_array() : void
		{
			$object = TestEnumHasFlags::make(['FIRST', 'SECOND']);
			self::assertTrue($object->has(TestEnumHasFlags::FIRST()));
			self::assertTrue($object->has(TestEnumHasFlags::SECOND()));
			self::assertTrue($object->eq(TestEnumHasFlags::BOTH()));
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_make_flags_from_int() : void
		{
			$object = TestEnumHasFlags::make(0);
			self::assertNotTrue($object->has(TestEnumHasFlags::FIRST()));
			self::assertNotTrue($object->has(TestEnumHasFlags::SECOND()));
			self::assertTrue($object->eq(TestEnumHasFlags::NONE()));
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_make_flags_combination() : void
		{
			/** @var int $second */
			$second = TestEnumHasFlags::SECOND;

			/** @var int $third */
			$third = TestEnumHasFlags::THIRD;

			$object = TestEnumHasFlags::make($second | $third);
			self::assertNotTrue($object->has(TestEnumHasFlags::FIRST()));
			self::assertTrue($object->has(TestEnumHasFlags::THIRD()));
		}
	}
}