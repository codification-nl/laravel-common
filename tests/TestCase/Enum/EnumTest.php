<?php

namespace Codification\Common\Test\TestCase\Enum
{
	use Codification\Common\TestSuite\TestCase;

	class EnumTest extends TestCase
	{
		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_construct() : void
		{
			$object = TestEnum::make('hello');
			self::assertTrue($object->eq(TestEnum::HELLO()));
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\ParseException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_parse() : void
		{
			$object = TestEnum::parse('HELLO');
			self::assertTrue($object->eq(TestEnum::HELLO()));
			self::assertNotTrue($object->eq(TestEnum::WORLD()));
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\ParseException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_throws_on_invalid_parse() : void
		{
			$this->expectException(\Codification\Common\Enum\Exceptions\ParseException::class);
			TestEnum::parse('abc');
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_throws_on_invalid_value() : void
		{
			$this->expectException(\Codification\Common\Enum\Exceptions\ValueException::class);
			TestEnum::make('abc');
		}

		/**
		 * @noinspection PhpDocRedundantThrowsInspection
		 * @test
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \Codification\Common\Enum\Exceptions\ParseException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 */
		public function it_throws_on_invalid() : void
		{
			$this->expectException(\Codification\Common\Enum\Exceptions\ParseException::class);
			/** @noinspection PhpUndefinedMethodInspection */
			TestEnum::ABC();
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_can_cast_to_string() : void
		{
			$object = TestEnum::HELLO();
			self::assertEquals('hello', (string)$object);
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_can_encode_to_json() : void
		{
			$object = TestEnum::HELLO();
			self::assertEquals(\json_encode('hello', \JSON_THROW_ON_ERROR), \json_encode($object, \JSON_THROW_ON_ERROR));
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_compare() : void
		{
			$object = TestEnum::HELLO();

			self::assertTrue($object->eq(TestEnum::HELLO()));
			self::assertTrue($object->eq(TestEnum::HELLO_ALSO()));
			self::assertTrue($object->eq(TestEnumFooBar::HELLO(), false));

			self::assertNotTrue($object->eq(TestEnum::WORLD()));
			self::assertNotTrue($object->eq(TestEnumFooBar::HELLO()));
		}

		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 */
		public function it_throws_on_assert() : void
		{
			$this->expectException(\Codification\Common\Enum\Exceptions\TypeException::class);
			TestEnum::assertType(TestEnumFooBar::HELLO());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\Exception
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_have_hidden() : void
		{
			$keys = TestEnum::keys();
			self::assertArrayNotHasKey('NONE', $keys);
		}
	}
}