<?php

namespace Codification\Common\Test\TestCase\Country
{
	use Codification\Common\Country\Exceptions\CountryCodeException;
	use Codification\Common\TestSuite\TestCase;
	use Codification\Common\Country\Country;

	class ValidationTest extends TestCase
	{
		/**
		 * @test
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_can_validate() : void
		{
			self::assertTrue(Country::isValid('nl'));
			self::assertNotTrue(Country::isValid('abc'));
		}

		/**
		 * @test
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 */
		public function it_can_assert() : void
		{
			$this->expectException(CountryCodeException::class);
			Country::ensureValid('abc');
		}
	}
}