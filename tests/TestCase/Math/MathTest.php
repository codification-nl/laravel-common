<?php

namespace Codification\Common\Test\TestCase\Math
{
	use Codification\Common\TestSuite\TestCase;
	use Codification\Common\Math\Math;
	use Codification\Common\Math\Number;

	class MathTest extends TestCase
	{
		/**
		 * @test
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_add_value_type() : void
		{
			$object = Math::add(Number::make(1, 8), 0.6);
			self::assertEquals('1.6', $object->getValue());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_add_number() : void
		{
			$object = Math::add(Number::make(1, 8), Number::make(0.6));
			self::assertEquals('1.6', $object->getValue());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_have_scale() : void
		{
			$object = Math::div(Number::make(1, 2), 0.6);
			self::assertEquals('1.66', $object->getValue());
		}

		/**
		 * @test
		 * @throws \DivisionByZeroError
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_round() : void
		{
			$object = Math::round(Number::make(1.4, 8));
			self::assertEquals('1', $object->getValue());
			$object = Math::round(Number::make(-1.4, 8));
			self::assertEquals('-1', $object->getValue());

			$object = Math::round(Number::make(1.66, 8));
			self::assertEquals('2', $object->getValue());
			$object = Math::round(Number::make(-1.66, 8));
			self::assertEquals('-2', $object->getValue());
		}

		/**
		 * @test
		 * @throws \DivisionByZeroError
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_round_up() : void
		{
			$object = Math::round(Number::make(1.66, 8), 0, Math::ROUND_UP);
			self::assertEquals('2', $object->getValue());
			$object = Math::round(Number::make(-1.66, 8), 0, Math::ROUND_UP);
			self::assertEquals('-1', $object->getValue());

			$object = Math::round(Number::make(1.66, 8), 1, Math::ROUND_UP);
			self::assertEquals('1.7', $object->getValue());
			$object = Math::round(Number::make(-1.66, 8), 1, Math::ROUND_UP);
			self::assertEquals('-1.6', $object->getValue());

			$object = Math::round(Number::make(1.66, 8), 2, Math::ROUND_UP);
			self::assertEquals('1.66', $object->getValue());
			$object = Math::round(Number::make(-1.66, 8), 2, Math::ROUND_UP);
			self::assertEquals('-1.66', $object->getValue());
		}

		/**
		 * @test
		 * @throws \DivisionByZeroError
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_round_down() : void
		{
			$object = Math::round(Number::make(1.66, 8), 0, Math::ROUND_DOWN);
			self::assertEquals('1', $object->getValue());
			$object = Math::round(Number::make(-1.66, 8), 0, Math::ROUND_DOWN);
			self::assertEquals('-2', $object->getValue());

			$object = Math::round(Number::make(1.66, 8), 1, Math::ROUND_DOWN);
			self::assertEquals('1.6', $object->getValue());
			$object = Math::round(Number::make(-1.66, 8), 1, Math::ROUND_DOWN);
			self::assertEquals('-1.7', $object->getValue());

			$object = Math::round(Number::make(1.66, 8), 2, Math::ROUND_DOWN);
			self::assertEquals('1.66', $object->getValue());
			$object = Math::round(Number::make(-1.66, 8), 2, Math::ROUND_DOWN);
			self::assertEquals('-1.66', $object->getValue());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_floor() : void
		{
			$object = Math::floor(Number::make(1.4, 8));
			self::assertEquals('1', $object->getValue());
			$object = Math::floor(Number::make(-1.4, 8));
			self::assertEquals('-2', $object->getValue());

			$object = Math::floor(Number::make(1.66, 8));
			self::assertEquals('1', $object->getValue());
			$object = Math::floor(Number::make(-1.66, 8));
			self::assertEquals('-2', $object->getValue());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_ceil() : void
		{
			$object = Math::ceil(Number::make(1.4, 8));
			self::assertEquals('2', $object->getValue());
			$object = Math::ceil(Number::make(-1.4, 8));
			self::assertEquals('-1', $object->getValue());

			$object = Math::ceil(Number::make(1.66, 8));
			self::assertEquals('2', $object->getValue());
			$object = Math::ceil(Number::make(-1.66, 8));
			self::assertEquals('-1', $object->getValue());
		}
	}
}