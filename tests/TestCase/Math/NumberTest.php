<?php

namespace Codification\Common\Test\TestCase\Math
{
	use Codification\Common\TestSuite\TestCase;
	use Codification\Common\Math\MathOp;
	use Codification\Common\Math\Number;

	class NumberTest extends TestCase
	{
		/**
		 * @test
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \PHPUnit\Framework\Exception
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_has_helper_function() : void
		{
			$object = \number(1);
			self::assertEquals('1', $object->getValue());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_add_value_type() : void
		{
			$object = Number::make(1, 8)->add(0.6);
			self::assertEquals('1.6', $object->getValue());

			$object = Number::make(1, 8);
			$object->add(0.6);
			self::assertNotEquals('1.6', $object->getValue());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_add_number() : void
		{
			$object = Number::make(1, 8)->add(Number::make(0.6));
			self::assertEquals('1.6', $object->getValue());

			$object = Number::make(1, 8);
			$object->add(Number::make(0.6));
			self::assertNotEquals('1.6', $object->getValue());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_have_scale() : void
		{
			$object = Number::make(1, 2)->div(0.6);
			self::assertEquals('1.66', $object->getValue());

			$object = Number::make(1, 2);
			$object->div(0.6);
			self::assertNotEquals('1.66', $object->getValue());
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_cast_to_string() : void
		{
			self::assertEquals((string)1, (string)Number::make(1));
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_encode_to_json() : void
		{
			self::assertEquals(\json_encode('1', \JSON_THROW_ON_ERROR), \json_encode(Number::make(1), \JSON_THROW_ON_ERROR));
		}

		/**
		 * @test
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function it_can_compare() : void
		{
			$scale = 8;

			$tests = [
				['1.0000000000', '0.6000000000'],
				['1.0000000000', '1.0000000000'],
				['1.6200000000', '1.6800000000'],
				['1.6666666699', '1.6666666611'],
			];

			$operators = [
				MathOp::EQ  => [false, true, false, true],
				MathOp::NE  => [true, false, true, false],
				MathOp::GT  => [true, false, false, false],
				MathOp::LT  => [false, false, true, false],
				MathOp::GTE => [true, true, false, true],
				MathOp::LTE => [false, true, true, true],
			];

			foreach ($operators as $operator => $results)
			{
				foreach ($tests as $index => [$a, $b])
				{
					$expected = $results[$index];

					/** @var \Codification\Common\Math\Number $object */
					$object = Number::make($a, $scale)->{$operator}($b);

					$result  = $expected ? 'true' : 'false';
					$message = "{$operator}[{$index}] => (Number::make('{$a}', {$scale})->{$operator}('{$b}') === {$result})";

					self::assertEquals($expected, $object, $message);
				}
			}
		}
	}
}