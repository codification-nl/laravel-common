<?php

namespace Codification\Common\Test\TestCase
{
	use Codification\Common\TestSuite\TestCase;

	class HelpersTest extends TestCase
	{
		/**
		 * @test
		 * @throws \PHPUnit\Framework\AssertionFailedError
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function str_common() : void
		{
			self::assertEquals(\str_common(['interspecies', 'interstelar', 'interstate']), 'inters');
			self::assertEquals(\str_common(['throne', 'throne']), 'throne');
			self::assertEquals(\str_common(['throne', 'dungeon']), '');
			self::assertEquals(\str_common(['cheese']), '');
			self::assertEquals(\str_common([]), '');
			self::assertEquals(\str_common(['prefix', 'suffix']), '');
		}
	}
}