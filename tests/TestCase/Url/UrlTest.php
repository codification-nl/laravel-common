<?php

namespace Codification\Common\Test\TestCase\Url
{
	use Codification\Common\TestSuite\TestCase;

	class UrlTest extends TestCase
	{
		/**
		 * @test
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \Codification\Common\Url\Exceptions\InvalidUrlException
		 * @throws \PHPUnit\Framework\ExpectationFailedException
		 * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
		 */
		public function it_has_helper_function() : void
		{
			$object = \url_parse('https://codification.nl/');
			self::assertEquals('codification.nl', $object->host);
		}
	}
}