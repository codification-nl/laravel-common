<?php

namespace Codification\Common\Enum
{
	/**
	 * @mixin \Codification\Common\Enum\Enum<int>
	 */
	trait EnumFlags
	{
		/**
		 * @param int|string|array<string> $value
		 * @psalm-param numeric|string|list<string> $value
		 *
		 * @return int
		 */
		public static function initializeEnumFlags($value) : int
		{
			if (\is_numeric($value))
			{
				return (int)$value;
			}

			if (empty($value))
			{
				return 0;
			}

			if (!\is_array($value))
			{
				$value = \explode(',', \strtoupper($value));
			}

			/*@return \array_reduce($value, static fn(int $result, string $string) : int => ($result | static::parse($string)->getValue()), 0);*/

			return \array_reduce($value, static function (int $result, string $string) : int
				{
					return ($result | static::parse($string)->getValue());
				}, 0);
		}

		/**
		 * @param array<string, int> $values
		 *
		 * @return array<string, int>
		 */
		public static function toArrayEnumFlags(array $values) : array
		{
			$keys  = array_diff(array_keys($values), static::$hidden);
			$count = 1 << \count($keys);

			for ($value = 1; $value < $count; $value++)
			{
				if (\in_array($value, $values, true))
				{
					continue;
				}

				/** @var string[] $pieces */
				$pieces = [];

				foreach ($keys as $key)
				{
					if (($value & $values[$key]) !== 0)
					{
						$pieces[] = $key;
					}
				}

				$key = \implode('_', $pieces);

				$values[$key]     = $value;
				static::$hidden[] = $key;
			}

			return $values;
		}

		/**
		 * @param \Codification\Common\Enum\Enum $enum
		 * @psalm-param \Codification\Common\Enum\Enum<int> $enum
		 *
		 * @return bool
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 */
		public function has($enum) : bool
		{
			static::assertType($enum);

			return (($this->value & $enum->getValue()) !== 0);
		}

		/**
		 * @param \Codification\Common\Enum\Enum ...$enums
		 * @psalm-param \Codification\Common\Enum\Enum<int> ...$enums
		 *
		 * @return $this
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 */
		public function set(...$enums) : self
		{
			/*@return $this->op($enums, static fn(int $lhs, int $rhs) : int => ($lhs | $rhs));*/
			return $this->op($enums, static function (int $lhs, int $rhs) : int
				{
					return ($lhs | $rhs);
				});
		}

		/**
		 * @param \Codification\Common\Enum\Enum ...$enums
		 * @psalm-param \Codification\Common\Enum\Enum<int> ...$enums
		 *
		 * @return $this
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 */
		public function remove(...$enums) : self
		{
			/*@return $this->op($enums, static fn(int $lhs, int $rhs) : int => ($lhs & ~$rhs));*/
			return $this->op($enums, static function (int $lhs, int $rhs) : int
				{
					return ($lhs & ~$rhs);
				});
		}

		/**
		 * @param \Codification\Common\Enum\Enum ...$enums
		 * @psalm-param \Codification\Common\Enum\Enum<int> ...$enums
		 *
		 * @return $this
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 */
		public function toggle(...$enums) : self
		{
			/*@return $this->op($enums, static fn(int $lhs, int $rhs) : int => ($lhs ^ $rhs));*/
			return $this->op($enums, static function (int $lhs, int $rhs) : int
				{
					return ($lhs ^ $rhs);
				});
		}

		/**
		 * @param \Codification\Common\Enum\Enum[] $enums
		 * @psalm-param list<\Codification\Common\Enum\Enum<int>> $enums
		 * @param \Closure                         $op
		 * @psalm-param \Closure(int, int):int     $op
		 * @return $this
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 */
		private function op(array $enums, \Closure $op) : self
		{
			foreach ($enums as $enum)
			{
				static::assertType($enum);

				$this->value = $op($this->value, $enum->getValue());
			}

			return $this;
		}
	}
}