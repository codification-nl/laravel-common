<?php

namespace Codification\Common\Enum
{
	use Codification\Common\Contracts;
	use Codification\Common\Support;
	use Codification\Common\Validation;

	/**
	 * @template T of array-key
	 */
	abstract class Enum implements Contracts\Support\Stringable
	{
		/**
		 * @psalm-var T
		 * @var int|string
		 */
		protected $value;

		/** @var array<string, array<string, array-key>> */
		protected static /*@array*/
			$cache = [];

		/**
		 * @psalm-var list<string>
		 * @var string[]
		 */
		protected static /*@array*/
			$hidden = [];

		/**
		 * @psalm-return T
		 * @return int|string
		 */
		public function getValue()
		{
			return $this->value;
		}

		/**
		 * @psalm-return class-string<\Codification\Common\Enum\Enum<T>>
		 * @return string|\Codification\Common\Enum\Enum
		 */
		public function getClass() : string
		{
			/**
			 * @psalm-var class-string<\Codification\Common\Enum\Enum<T>> $class
			 * @var string|\Codification\Common\Enum\Enum $class
			 */
			$class = static::class;

			return $class;
		}

		/**
		 * @psalm-param T    $value
		 * @param int|string $value
		 *
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		protected function __construct($value)
		{
			if (!static::isValid($value))
			{
				throw new Exceptions\ValueException($value, static::class);
			}

			/** @psalm-var T $value */
			$this->value = $value;
		}

		/**
		 * @psalm-param \Codification\Common\Enum\Enum<T>|T $enum
		 * @param \Codification\Common\Enum\Enum|int|string $enum
		 * @param bool                                      $strict = true
		 *
		 * @return bool
		 *
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function equals($enum, bool $strict = true) : bool
		{
			if (!\is_object($enum))
			{
				$enum = static::make($enum);
			}

			if ($strict && ($this->getClass() !== $enum->getClass()))
			{
				return false;
			}

			return ($this->value === $enum->value);
		}

		/**
		 * @psalm-param \Codification\Common\Enum\Enum<T>|T $enum
		 * @param \Codification\Common\Enum\Enum|int|string $enum
		 * @param bool                                      $strict = true
		 *
		 * @return bool
		 *
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function eq($enum, bool $strict = true) : bool
		{
			return $this->equals($enum, $strict);
		}

		/**
		 * @template    TValue of array-key
		 *
		 * @psalm-param TValue    $value
		 * @param int|string $value
		 * @param bool       $strict = true
		 *
		 * @return bool
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function isValid($value, bool $strict = true) : bool
		{
			return \in_array($value, static::toArray(), $strict);
		}

		/**
		 * @template     TValue of array-key
		 *
		 * @psalm-return list<TValue>
		 * @return int[]|string[]
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function values() : array
		{
			/**
			 * @psalm-var list<TValue>
			 * @var int[]|string[] $values
			 */
			$values = \array_values(static::toArray());

			return $values;
		}

		/**
		 * @psalm-return list<string>
		 * @return string[]
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function keys() : array
		{
			/**
			 * @psalm-var list<string> $keys
			 * @var string[] $keys
			 */
			$keys = \array_diff(\array_keys(static::toArray()), static::$hidden);

			return $keys;
		}

		/**
		 * @template TValue of array-key
		 *
		 * @psalm-param \Codification\Common\Enum\Enum<TValue> $enum
		 * @param \Codification\Common\Enum\Enum $enum
		 *
		 * @return void
		 *
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 */
		public static function assertType(Enum $enum) : void
		{
			$type  = static::class;
			$other = $enum->getClass();

			if ($other !== $type)
			{
				throw new Exceptions\TypeException("[{$other}] !== [{$type}]");
			}
		}

		/**
		 * @template     TValue of array-key
		 *
		 * @psalm-return array<string, TValue>
		 * @return array<string, int|string>
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function toArray() : array
		{
			$type = static::class;

			if (!array_key_exists($type, static::$cache))
			{
				try
				{
					$reflection = new \ReflectionClass($type);
				}
				catch (\ReflectionException $e)
				{
					throw new Support\Exceptions\ShouldNotHappenException("[{$type}] does not exist", $e);
				}

				/**
				 * @psalm-var  array<string, TValue> $values
				 * @var array<string, int|string> $values
				 */
				$values = $reflection->getConstants();

				static::$cache[$type] = static::toArrayTraits($values);
			}

			/**
			 * @psalm-var array<string, TValue>
			 * @var array<string, int|string> $array
			 */
			$array = static::$cache[$type];

			return $array;
		}

		/**
		 * @template     TValue of array-key
		 *
		 * @psalm-param  TValue|list<string> $value
		 * @param int|string|string[] $value
		 *
		 * @psalm-return static<TValue>
		 * @return static
		 *
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function make($value) : Enum
		{
			/**
			 * @psalm-var TValue $value
			 * @var int|string $value
			 */
			$value = static::initializeTraits($value);

			/**
			 * @psalm-var static<TValue> $enum
			 * @var static $enum
			 */
			$enum = new static($value);

			return $enum;
		}

		/**
		 * @template     TValue of array-key
		 * @template     TIn as TValue|array<string, TValue>|list<string>
		 * @template     TOut as TValue|array<string, TValue>
		 *
		 * @psalm-param  TIn                                   $value
		 * @param int|string|array<string,int|string>|string[] $value
		 * @param string                                       $to
		 * @psalm-param  null|\Closure(TIn|TOut,TOut):TOut     $cb = null
		 * @param null|\Closure                                $cb = null
		 *
		 * @psalm-return TOut
		 * @return int|string|array<string,int|string>
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		private static function forward($value, string $to, \Closure $cb = null)
		{
			$class = static::class;

			foreach (\class_traits($class) as $trait)
			{
				$trait_name  = \class_basename($trait);
				$method_name = "{$to}{$trait_name}";

				if (!\method_exists($class, $method_name))
				{
					continue;
				}

				/**
				 * @psalm-var TOut|false $trait_value
				 * @var int|string|array<string,int|string>|false $trait_value
				 */
				$trait_value = \forward_static_call([$class, $method_name], $value);

				if ($trait_value === false)
				{
					throw new Support\Exceptions\ShouldNotHappenException("Failed to call [{$class}::{$method_name}]");
				}

				if ($cb !== null)
				{
					$trait_value = $cb($value, $trait_value);
				}

				/**
				 * @psalm-var TOut $trait_value
				 * @var int|string|array<string,int|string> $trait_value
				 */
				$value = $trait_value;
			}

			/** @psalm-var TOut $value */
			return $value;
		}

		/**
		 * @template     TValue of array-key
		 *
		 * @psalm-param  TValue|list<string> $value
		 * @param int|string|string[] $value
		 *
		 * @psalm-return TValue
		 * @return int|string
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		private static function initializeTraits($value)
		{
			/**
			 * @psalm-var TValue $result
			 * @var int|string $result
			 */
			$result = static::forward($value, 'initialize');

			return $result;
		}

		/**
		 * @template     TValue of array-key
		 *
		 * @psalm-param  array<string,TValue> $values
		 * @param array<string,int|string> $values
		 *
		 * @psalm-return array<string,TValue>
		 * @return array<string,int|string>
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		private static function toArrayTraits(array $values) : array
		{
			/**
			 * @psalm-var array<string, TValue> $result
			 * @var array<string, int|string> $result
			 */
			$result = static::forward(
				$values,
				'toArray',
				/**
				 * @psalm-param  array<string, TValue>   $values
				 * @param array<string, int|string> $values
				 * @psalm-param  array<string, TValue>   $trait_values
				 * @param array<string, int|string> $trait_values
				 *
				 * @psalm-return array<string, TValue>
				 * @return array<string, int|string>
				 */
				static function (array $values, array $trait_values) : array
					{
						return \array_merge($trait_values, $values);
					}
			/*@static fn(array $values, array $trait_values) : array => \array_merge($trait_values, $values)*/
			);

			return $result;
		}

		/**
		 * @template     TValue of array-key
		 *
		 * @param string $name
		 *
		 * @psalm-return static<TValue>
		 * @return static
		 *
		 * @throws \Codification\Common\Enum\Exceptions\ParseException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function parse(string $name) : Enum
		{
			$values = static::toArray();

			if (!array_key_exists($name, $values))
			{
				throw new Exceptions\ParseException($name, static::class);
			}

			/**
			 * @psalm-var      TValue $value
			 * @var int|string $value
			 * @psalm-suppress EmptyArrayAccess
			 */
			$value = $values[$name];

			return new static($value);
		}

		/**
		 * @template     TValue of array-key
		 *
		 * @param string $name
		 * @param array  $parameters
		 *
		 * @psalm-return static<TValue>
		 * @return static
		 *
		 * @throws \Codification\Common\Enum\Exceptions\ParseException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function __callStatic(string $name, array $parameters)
		{
			return static::parse($name);
		}

		/**
		 * @return string
		 */
		public function toString() : string
		{
			return (string)$this->getValue();
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->toString();
		}

		/**
		 * @psalm-return T
		 * @return int|string
		 */
		public function jsonSerialize()
		{
			return $this->getValue();
		}

		/**
		 * @param bool $strict = Validation\Rules\EnumRule::STRICT
		 *
		 * @return \Codification\Common\Validation\Rules\EnumRule
		 */
		public static function rule(bool $strict = Validation\Rules\EnumRule::DEFAULT_STRICT) : Validation\Rules\EnumRule
		{
			return Validation\Rules\EnumRule::make(static::class, $strict);
		}
	}
}