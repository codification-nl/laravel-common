<?php

namespace Codification\Common\Enum\Exceptions
{
	final class ParseException extends \RuntimeException
	{
		/**
		 * @param string $name
		 * @param string $enum
		 * @psalm-param class-string<\Codification\Common\Enum\Enum> $enum
		 */
		public function __construct(string $name, string $enum)
		{
			parent::__construct("'{$name}' does not exist in [{$enum}]");
		}
	}
}