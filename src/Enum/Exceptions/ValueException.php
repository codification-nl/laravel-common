<?php

namespace Codification\Common\Enum\Exceptions
{
	final class ValueException extends \DomainException
	{
		/**
		 * @param int|string $value
		 * @psalm-param array-key $value
		 * @param string     $enum
		 * @psalm-param class-string<\Codification\Common\Enum\Enum> $enum
		 */
		public function __construct($value, string $enum)
		{
			parent::__construct("Unexpected value '{$value}' for [{$enum}]");
		}
	}
}