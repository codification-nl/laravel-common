<?php

if (!\function_exists('sanitize'))
{
	/**
	 * @param mixed    $value
	 * @param int|null $filter = null
	 * @psalm-param int|null = null
	 * @return string|null
	 */
	function sanitize($value, int $filter = null) : ?string
	{
		if ($filter === null)
		{
			$value = \trim((string)$value);

			if ($value === '')
			{
				return null;
			}

			return $value;
		}

		/** @var string|false $value */
		$value = \filter_var($value, $filter);

		if ($value === false)
		{
			return null;
		}

		return $value;
	}
}

if (!\function_exists('url_parse'))
{
	/**
	 * @param string                                     $url
	 * @param \Codification\Common\Url\UrlParseFlags|int $flags
	 *
	 * @return \Codification\Common\Url\UrlParser
	 * @throws \Codification\Common\Enum\Exceptions\TypeException
	 * @throws \Codification\Common\Enum\Exceptions\ValueException
	 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
	 * @throws \Codification\Common\Url\Exceptions\InvalidUrlException
	 */
	function url_parse(string $url, $flags = \Codification\Common\Url\UrlParseFlags::ALL) : \Codification\Common\Url\UrlParser
	{
		return \Codification\Common\Url\UrlParser::parse($url, $flags);
	}
}

if (!\function_exists('money'))
{
	/**
	 * @param string|float|int|null  $value
	 * @psalm-param numeric|null     $value
	 * @param string|\Money\Currency $currency
	 * @param string|null            $locale = null
	 *
	 * @return \Codification\Common\Money\Money|null
	 * @throws \Codification\Common\Money\Exceptions\CurrencyCodeException
	 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
	 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
	 * @throws \Codification\Common\Money\Exceptions\ParseException
	 */
	function money($value, $currency, string $locale = null) : ?\Codification\Common\Money\Money
	{
		return \Codification\Common\Money\Money::make($value, $currency, $locale);
	}
}

if (!\function_exists('phone'))
{
	/**
	 * @param string $number
	 * @param string $region_code
	 *
	 * @return \Codification\Common\Phone\Phone|null
	 * @throws \Codification\Common\Enum\Exceptions\ValueException
	 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
	 */
	function phone($number, $region_code) : ?\Codification\Common\Phone\Phone
	{
		return \Codification\Common\Phone\Phone::make($number, $region_code);
	}
}

if (!\function_exists('number'))
{
	/**
	 * @param int|float|string $value
	 * @param int|null         $scale = null
	 *
	 * @return \Codification\Common\Math\Number
	 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
	 */
	function number($value, int $scale = null) : \Codification\Common\Math\Number
	{
		return new \Codification\Common\Math\Number($value, $scale);
	}
}

if (!\function_exists('array_value'))
{
	/**
	 * @template     T of array
	 * @template     TKey as key-of<T>
	 * @template     TValue
	 *
	 * @psalm-param  T   $array
	 * @param array      $array
	 * @psalm-param  TKey $key
	 * @param int|string $key
	 * @psalm-param  TValue   $default = null
	 * @param mixed      $default = null
	 *
	 * @psalm-return T[TKey]|mixed
	 * @return mixed
	 */
	function array_value(array $array, $key, $default = null)
	{
		return $array[$key] ?? $default;
	}
}

if (!\function_exists('array_unassoc'))
{
	/**
	 * @param string     $key
	 * @param string     $value
	 * @psalm-param array<string, mixed> $array
	 * @param array      $array
	 * @psalm-param list<string>|null $keys = null
	 * @param array|null $keys = null
	 *
	 * @return mixed
	 */
	function array_unassoc(string $key, string $value, array $array, array $keys = null)
	{
		/** @psalm-suppress MissingParamType */
		/*@return \array_map(
			static fn($k, $v) : array => [$key => $k, $value => $v,],
			$keys ?? \array_keys($array),
			$array
		);*/

		return \array_map(
		/** @psalm-suppress MissingClosureParamType */
			static function ($k, $v) use ($key, $value) : array
				{
					return [$key => $k, $value => $v,];
				},
			$keys ?? \array_keys($array),
			$array
		);
	}
}

if (!\function_exists('array_map_keys'))
{
	/**
	 * @template     TIn
	 * @template     TOut
	 *
	 * @psalm-param \Closure(TIn, string):array<string, TOut> $callback
	 * @param \Closure $callback
	 * @psalm-param  array<string, TIn> $array
	 * @param array    $array
	 *
	 * @psalm-return array<string, TOut>
	 * @return array
	 */
	function array_map_keys(\Closure $callback, array $array) : array
	{
		$result = [];

		foreach ($array as $key => $value)
		{
			$mapped = $callback($value, $key);

			foreach ($mapped as $map_key => $map_value)
			{
				$result[$map_key] = $map_value;
			}
		}

		return $result;
	}
}

if (!\function_exists('array_reduce_assoc'))
{
	/**
	 * @template     T
	 *
	 * @psalm-param  array<string, T> $array
	 * @param array    $array
	 * @psalm-param \Closure(T, T, string):T $callback
	 * @param \Closure $callback
	 * @psalm-param  T $initial
	 * @param mixed    $initial
	 *
	 * @psalm-return T
	 * @return mixed
	 */
	function array_reduce_assoc(array $array, \Closure $callback, $initial)
	{
		$result = $initial;

		foreach ($array as $key => $value)
		{
			$result = $callback($result, $value, $key);
		}

		return $result;
	}
}

if (!\function_exists('urlsafe_base64'))
{
	/**
	 * @param int  $length  = 16
	 * @param bool $padding = false
	 *
	 * @return string
	 *
	 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
	 */
	function urlsafe_base64(int $length = 16, bool $padding = false) : string
	{
		return \Codification\Common\Support\SecureRandom::urlsafe_base64($length, $padding);
	}
}

if (!\function_exists('class_traits'))
{
	/**
	 * @template     T as object
	 *
	 * @psalm-param  class-string<T>|T $class
	 * @param string|object $class
	 *
	 * @psalm-return list<class-string>
	 * @return array<string|object>
	 */
	function class_traits($class) : array
	{
		/**
		 * @psalm-var list<class-string> $traits
		 * @var array<string|object> $traits
		 */
		$traits = \class_uses_recursive($class);

		return $traits;
	}
}

if (!\function_exists('class_interfaces'))
{
	/**
	 * @template     T as object
	 *
	 * @psalm-param  class-string<T>|T $class
	 * @param string|object $class
	 *
	 * @psalm-return list<class-string>
	 * @return array<string|object>
	 */
	function class_interfaces($class) : array
	{
		/**
		 * @psalm-var list<class-string>|false $interfaces
		 * @var array<string|object>|false $interfaces
		 */
		$interfaces = \class_implements($class);

		return $interfaces ?: [];
	}
}

if (!\function_exists('str_common'))
{
	/**
	 * @psalm-param list<string> $array
	 * @param array $array
	 *
	 * @return string
	 *
	 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
	 */
	function str_common(array $array) : string
	{
		if (\count($array) <= 1)
		{
			return '';
		}

		\sort($array, \SORT_NATURAL);

		/** @var string $a */
		$a = \array_shift($array);
		/** @var string $b */
		$b = \array_pop($array);

		$index  = 0;
		$length = \strlen($a);

		while ($index < $length && $a[$index] === $b[$index])
		{
			$index++;
		}

		$result = \substr($a, 0, $index);

		if ($result === false)
		{
			throw new \Codification\Common\Support\Exceptions\ShouldNotHappenException('Failed to part of string');
		}

		return $result;
	}
}

if (!\function_exists('class_name_short'))
{
	/**
	 * @template    T as object
	 *
	 * @psalm-param class-string<T>|T $class
	 * @param string|object $class
	 *
	 * @return string|false
	 */
	function class_name_short($class)
	{
		if (\is_object($class))
		{
			$class = \get_class($class);
		}

		$class = \strrchr($class, '\\');

		if ($class === false)
		{
			return false;
		}

		$class = \substr($class, 1);

		if ($class === false)
		{
			return false;
		}

		return $class;
	}
}