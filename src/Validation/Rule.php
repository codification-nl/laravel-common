<?php

namespace Codification\Common\Validation
{
	final class Rule
	{
		/**
		 * @return \Codification\Common\Validation\Rules\AlphaSpacesRule
		 */
		public static function alpha_spaces() : Rules\AlphaSpacesRule
		{
			return Rules\AlphaSpacesRule::make();
		}

		/**
		 * @return \Codification\Common\Validation\Rules\CountryRule
		 */
		public static function country() : Rules\CountryRule
		{
			return Rules\CountryRule::make();
		}

		/**
		 * @param string|null $from    = null
		 * @param int         $timeout = Rules\EmailRule::DEFAULT_TIMEOUT
		 *
		 * @return \Codification\Common\Validation\Rules\EmailRule
		 */
		public static function email(string $from = null, int $timeout = Rules\EmailRule::DEFAULT_TIMEOUT) : Rules\EmailRule
		{
			return Rules\EmailRule::make($from, $timeout);
		}

		/**
		 * @template    TValue of int|string
		 * @param string|\Codification\Common\Enum\Enum $enum
		 * @psalm-param class-string<\Codification\Common\Enum\Enum<TValue>> $enum
		 * @param bool                                  $strict = Rules\EnumRule::STRICT
		 *
		 * @return \Codification\Common\Validation\Rules\EnumRule
		 */
		public static function enum(string $enum, bool $strict = Rules\EnumRule::DEFAULT_STRICT) : Rules\EnumRule
		{
			return Rules\EnumRule::make($enum, $strict);
		}

		/**
		 * @param bool $allow_empty = Rules\IntervalRule::ALLOW_EMPTY
		 *
		 * @return \Codification\Common\Validation\Rules\IntervalRule
		 */
		public static function interval(bool $allow_empty = Rules\IntervalRule::DEFAULT_ALLOW_EMPTY) : Rules\IntervalRule
		{
			return Rules\IntervalRule::make($allow_empty);
		}

		/**
		 * @return \Codification\Common\Validation\Rules\PeriodRule
		 */
		public static function period() : Rules\PeriodRule
		{
			return Rules\PeriodRule::make();
		}

		/**
		 * @param string|null $country_field = null
		 *
		 * @return \Codification\Common\Validation\Rules\PhoneRule
		 */
		public static function phone(string $country_field = null) : Rules\PhoneRule
		{
			return Rules\PhoneRule::make($country_field);
		}
	}
}