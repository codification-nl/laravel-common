<?php

namespace Codification\Common\Validation
{
	use Codification\Common\Support;

	final class Validator
	{
		/**
		 * @var array<string, string>
		 * @psalm-var array<string, class-string<\Codification\Common\Validation\Contracts\Validator>>
		 */
		public static /*@array*/
			$rules = [
			'alpha_spaces' => Rules\AlphaSpacesRule::class,
			'country'      => Rules\CountryRule::class,
			'email_plus'   => Rules\EmailRule::class,
			'enum'         => Rules\EnumRule::class,
			'interval'     => Rules\IntervalRule::class,
			'period'       => Rules\PeriodRule::class,
			'phone'        => Rules\PhoneRule::class,
		];

		/**
		 * @return void
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function extend() : void
		{
			try
			{
				/** @var \Illuminate\Validation\Factory $factory */
				$factory = Support\ContainerUtils::resolve('validator');
			}
			catch (Support\Exceptions\ResolutionException $e)
			{
				throw new Support\Exceptions\ShouldNotHappenException('Failed to resolve [validator]', $e);
			}

			foreach (self::$rules as $rule => $validator)
			{
				$interfaces = \class_interfaces($validator);

				if (\in_array(Contracts\ValidatorRule::class, $interfaces, true))
				{
					if (\in_array(Contracts\ValidatorDependent::class, $interfaces, true))
					{
						$factory->extendDependent($rule, "{$validator}@validate");
					}
					else
					{
						$factory->extend($rule, "{$validator}@validate");
					}
				}

				if (\in_array(Contracts\ValidatorReplacer::class, $interfaces, true))
				{
					$factory->replacer($rule, "{$validator}@replace");
				}
			}
		}
	}
}