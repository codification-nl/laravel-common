<?php

namespace Codification\Common\Validation\Rules
{
	use Codification\Common\Validation;
	use Illuminate;

	final class EnumRule implements Validation\Contracts\ValidatorRule, Validation\Contracts\ValidatorReplacer, Validation\Contracts\ValidatorDependent
	{
		/** @var bool */
		public const DEFAULT_STRICT = true;

		/**
		 * @psalm-var class-string<\Codification\Common\Enum\Enum>|null
		 * @var string|\Codification\Common\Enum\Enum|null
		 */
		protected /*@?string*/
			$enum = null;

		/** @var bool */
		protected /*@bool*/
			$strict = EnumRule::DEFAULT_STRICT;

		/**
		 * @psalm-param class-string<\Codification\Common\Enum\Enum> $enum
		 * @param string|\Codification\Common\Enum\Enum $enum
		 * @param bool                                  $strict = EnumRule::STRICT
		 *
		 * @return \Codification\Common\Validation\Rules\EnumRule
		 */
		public static function make(string $enum, bool $strict = EnumRule::DEFAULT_STRICT) : self
		{
			$rule = new self();

			$rule->enum   = $enum;
			$rule->strict = $strict;

			return $rule;
		}

		/**
		 * @param string                           $attribute
		 * @param string                           $value
		 * @param string[]                         $parameters
		 * @param \Illuminate\Validation\Validator $validator
		 *
		 * @return bool
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function validate(string $attribute, $value, array $parameters, Illuminate\Validation\Validator $validator) : bool
		{
			/**
			 * @psalm-var class-string<\Codification\Common\Enum\Enum> $enum
			 * @var string|\Codification\Common\Enum\Enum $enum
			 */
			[$enum, $strict] = $parameters;

			return $enum::isValid($value, (bool)$strict);
		}

		/**
		 * @param string                           $message
		 * @param string                           $attribute
		 * @param string                           $rule
		 * @param string[]                         $parameters
		 * @param \Illuminate\Validation\Validator $validator
		 *
		 * @return string
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function replace(string $message, string $attribute, string $rule, array $parameters, Illuminate\Validation\Validator $validator) : string
		{
			/**
			 * @psalm-var class-string<\Codification\Common\Enum\Enum> $enum
			 * @var string|\Codification\Common\Enum\Enum $enum
			 */
			[$enum] = $parameters;

			/** @var string $string */
			$string = \str_replace(':values', \implode(', ', $enum::values()), $message);

			return $string;
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return "enum:{$this->enum},{$this->strict}";
		}
	}
}