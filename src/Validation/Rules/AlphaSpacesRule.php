<?php

/** @noinspection RegExpUnexpectedAnchor */

namespace Codification\Common\Validation\Rules
{
	use Codification\Common\Support;
	use Codification\Common\Validation;
	use Illuminate;

	final class AlphaSpacesRule implements Validation\Contracts\ValidatorRule
	{
		/** @var string */
		public const PATTERN = /** @lang RegExp */
			"/^[\p{L}\p{M} '.\-]+$/u";

		/**
		 * @return \Codification\Common\Validation\Rules\AlphaSpacesRule
		 */
		public static function make() : self
		{
			return new self();
		}

		/**
		 * @param string                           $attribute
		 * @param string                           $value
		 * @psalm-param list<string>               $parameters
		 * @param string[]                         $parameters
		 * @param \Illuminate\Validation\Validator $validator
		 *
		 * @return bool
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function validate(string $attribute, $value, array $parameters, Illuminate\Validation\Validator $validator) : bool
		{
			$match = \preg_match(self::PATTERN, $value);

			if ($match === false)
			{
				throw new Support\Exceptions\ShouldNotHappenException('Failed to match');
			}

			return ($match === 1);
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return 'alpha_spaces';
		}
	}
}