<?php

namespace Codification\Common\Validation\Rules
{
	use Codification\Common\Phone;
	use Codification\Common\Validation;
	use Illuminate;

	final class PhoneRule implements Validation\Contracts\ValidatorRule, Validation\Contracts\ValidatorReplacer, Validation\Contracts\ValidatorDependent
	{
		/** @var int */
		private const DEFAULT_TYPE = Phone\PhoneType::BOTH;

		/** @var string|null */
		protected /*@?string*/
			$countryField = null;

		/** @var \Codification\Common\Phone\PhoneType|int|string */
		protected $type = PhoneRule::DEFAULT_TYPE;

		/**
		 * @param string|null                                     $country_field = null
		 * @param \Codification\Common\Phone\PhoneType|int|string $type          = PhoneRule::TYPE
		 *
		 * @return \Codification\Common\Validation\Rules\PhoneRule
		 */
		public static function make(string $country_field = null, $type = PhoneRule::DEFAULT_TYPE) : self
		{
			$rule = new self();

			$rule->countryField = $country_field;
			$rule->type         = $type;

			return $rule;
		}

		/**
		 * @return $this
		 */
		public function mobile() : self
		{
			/** @var int $type */
			$type = Phone\PhoneType::MOBILE;

			return $this->type($type);
		}

		/**
		 * @return $this
		 */
		public function fixed() : self
		{
			/** @var int $type */
			$type = Phone\PhoneType::FIXED;

			return $this->type($type);
		}

		/**
		 * @param \Codification\Common\Phone\PhoneType|int|string $type
		 *
		 * @return $this
		 */
		public function type($type) : self
		{
			$this->type = $type;

			return $this;
		}

		/**
		 * @param string $attribute
		 * @return string
		 */
		private static function getCountryField(string $attribute) : string
		{
			return "{$attribute}_country";
		}

		/**
		 * @param string                           $attribute
		 * @param string                           $value
		 * @psalm-param list<string>               $parameters
		 * @param string[]                         $parameters
		 * @param \Illuminate\Validation\Validator $validator
		 *
		 * @return bool
		 *
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function validate(string $attribute, $value, array $parameters, Illuminate\Validation\Validator $validator) : bool
		{
			/**
			 * @var string|null $country_field
			 * @var string|int  $type
			 */

			[$country_field, $type] = $parameters + [null, self::DEFAULT_TYPE];

			$target = $validator->getData();
			$key    = \sanitize($country_field) ?? self::getCountryField($attribute);

			/** @var string|null $region_code */
			$region_code = \data_get($target, $key, null);
			$region_code = \sanitize($region_code);

			if ($region_code === null)
			{
				return false;
			}

			return Phone\Phone::validate($value, $region_code, $type);
		}

		/**
		 * @param string                           $message
		 * @param string                           $attribute
		 * @param string                           $rule
		 * @psalm-param list<string>               $parameters
		 * @param string[]                         $parameters
		 * @param \Illuminate\Validation\Validator $validator
		 *
		 * @return string
		 */
		public static function replace(string $message, string $attribute, string $rule, array $parameters, Illuminate\Validation\Validator $validator) : string
		{
			/**
			 * @var string|null $country_field
			 */

			[$country_field] = $parameters + [null];

			$target = $validator->getData();
			$key    = \sanitize($country_field) ?? self::getCountryField($attribute);

			/** @var string|null $region_code */
			$region_code = \data_get($target, $key, null);
			$region_code = \sanitize($region_code);

			if ($region_code !== null)
			{
				$message = \str_replace(':country', $region_code, $message);
			}

			return $message;
		}

		/**
		 * @return string
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function __toString() : string
		{
			$type = $this->type;

			if (!($type instanceof Phone\PhoneType) && !Phone\PhoneType::isValid($type))
			{
				/** @var int $type */
				$type = self::DEFAULT_TYPE;
			}

			return "phone:{$this->countryField},{$type}";
		}
	}
}