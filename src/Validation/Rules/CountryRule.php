<?php

namespace Codification\Common\Validation\Rules
{
	use Codification\Common\Country;
	use Codification\Common\Validation;
	use Illuminate;

	final class CountryRule implements Validation\Contracts\ValidatorRule
	{
		/**
		 * @return \Codification\Common\Validation\Rules\CountryRule
		 */
		public static function make() : self
		{
			return new self();
		}

		/**
		 * @param string                           $attribute
		 * @param string|null                      $value
		 * @psalm-param list<string>               $parameters
		 * @param string[]                         $parameters
		 * @param \Illuminate\Validation\Validator $validator
		 *
		 * @return bool
		 */
		public static function validate(string $attribute, $value, array $parameters, Illuminate\Validation\Validator $validator) : bool
		{
			return Country\Country::isValid($value);
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return 'country';
		}
	}
}