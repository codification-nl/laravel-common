<?php

namespace Codification\Common\Validation\Rules
{
	use Carbon;
	use Illuminate;
	use Codification\Common\Validation;

	final class IntervalRule implements Validation\Contracts\ValidatorRule, Validation\Contracts\ValidatorDependent
	{
		/** @var bool */
		public const DEFAULT_ALLOW_EMPTY = false;

		/** @var bool */
		protected /*@bool*/
			$allowEmpty = IntervalRule::DEFAULT_ALLOW_EMPTY;

		/**
		 * @param bool $allow_empty = IntervalRule::ALLOW_EMPTY
		 *
		 * @return \Codification\Common\Validation\Rules\IntervalRule
		 */
		public static function make(bool $allow_empty = IntervalRule::DEFAULT_ALLOW_EMPTY) : self
		{
			$rule = new self();

			$rule->allowEmpty = $allow_empty;

			return $rule;
		}

		/**
		 * @param string                           $attribute
		 * @param string                           $value
		 * @psalm-param  list<string>              $parameters
		 * @param string[]                         $parameters
		 * @param \Illuminate\Validation\Validator $validator
		 *
		 * @return bool
		 *
		 * @noinspection BadExceptionsProcessingInspection
		 */
		public static function validate(string $attribute, $value, array $parameters, Illuminate\Validation\Validator $validator) : bool
		{
			try
			{
				$interval = Carbon\CarbonInterval::fromString($value);
			}
			catch (\Exception $e)
			{
				return false;
			}

			[$allow_empty] = $parameters + [self::DEFAULT_ALLOW_EMPTY];

			return ((bool)$allow_empty || !$interval->isEmpty());
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return "interval:{$this->allowEmpty}";
		}
	}
}