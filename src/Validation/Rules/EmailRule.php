<?php

namespace Codification\Common\Validation\Rules
{
	use Illuminate;
	use Codification\Common\Mail;
	use Codification\Common\Validation;

	final class EmailRule implements Validation\Contracts\ValidatorRule, Validation\Contracts\ValidatorDependent
	{
		/** @var int */
		public const DEFAULT_TIMEOUT = Mail\Email::SMTP_TIMEOUT;

		/** @var string|null */
		protected /*@?string*/
			$from = null;

		/** @var int */
		protected /*@int*/
			$timeout = EmailRule::DEFAULT_TIMEOUT;

		/**
		 * @param string|null $from    = null
		 * @param int         $timeout = EmailRule::DEFAULT_TIMEOUT
		 * @return \Codification\Common\Validation\Rules\EmailRule
		 */
		public static function make(string $from = null, int $timeout = EmailRule::DEFAULT_TIMEOUT) : self
		{
			$rule = new self();

			$rule->from    = $from;
			$rule->timeout = $timeout;

			return $rule;
		}

		/**
		 * @param string                           $attribute
		 * @param string                           $value
		 * @psalm-param list<string>               $parameters
		 * @param string[]                         $parameters
		 * @param \Illuminate\Validation\Validator $validator
		 *
		 * @return bool
		 * @throws \Codification\Common\Mail\Exceptions\EmailException
		 * @throws \Codification\Common\Support\Exceptions\ReferenceException
		 * @throws \Codification\Common\Support\Exceptions\ResolutionException
		 */
		public static function validate(string $attribute, $value, array $parameters, Illuminate\Validation\Validator $validator) : bool
		{
			/**
			 * @var string|null $from
			 * @var string      $timeout
			 */
			[$from, $timeout] = $parameters + [null, self::DEFAULT_TIMEOUT];

			return Mail\Email::isValid($value, $from, (int)$timeout);
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return "email_plus:{$this->from},{$this->timeout}";
		}
	}
}