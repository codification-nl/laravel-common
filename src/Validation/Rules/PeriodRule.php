<?php

namespace Codification\Common\Validation\Rules
{
	use Carbon;
	use Illuminate;
	use Codification\Common\Validation;

	final class PeriodRule implements Validation\Contracts\ValidatorRule
	{
		/**
		 * @return \Codification\Common\Validation\Rules\PeriodRule
		 */
		public static function make() : self
		{
			return new self();
		}

		/**
		 * @param string                           $attribute
		 * @param string                           $value
		 * @psalm-param  list<string>              $parameters
		 * @param string[]                         $parameters
		 * @param \Illuminate\Validation\Validator $validator
		 *
		 * @return bool
		 *
		 * @noinspection BadExceptionsProcessingInspection
		 */
		public static function validate(string $attribute, $value, array $parameters, Illuminate\Validation\Validator $validator) : bool
		{
			try
			{
				Carbon\CarbonPeriod::createFromIso($value);
			}
			catch (\Exception $e)
			{
				return false;
			}

			return true;
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return 'period';
		}
	}
}