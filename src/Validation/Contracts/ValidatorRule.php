<?php

namespace Codification\Common\Validation\Contracts
{
	use Illuminate;

	interface ValidatorRule extends Validator
	{
		/**
		 * @param string                           $attribute
		 * @param string                           $value
		 * @psalm-param list<string>               $parameters
		 * @param string[]                         $parameters
		 * @param \Illuminate\Validation\Validator $validator
		 *
		 * @return bool
		 */
		public static function validate(string $attribute, $value, array $parameters, Illuminate\Validation\Validator $validator) : bool;

		/**
		 * @return string
		 */
		public function __toString() : string;
	}
}