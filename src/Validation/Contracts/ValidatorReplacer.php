<?php

namespace Codification\Common\Validation\Contracts
{
	use Illuminate;

	interface ValidatorReplacer extends Validator
	{
		/**
		 * @param string                           $message
		 * @param string                           $attribute
		 * @param string                           $rule
		 * @psalm-param list<string>               $parameters
		 * @param string[]                         $parameters
		 * @param \Illuminate\Validation\Validator $validator
		 *
		 * @return string
		 */
		public static function replace(string $message, string $attribute, string $rule, array $parameters, Illuminate\Validation\Validator $validator) : string;
	}
}