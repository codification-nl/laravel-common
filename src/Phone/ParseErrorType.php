<?php

namespace Codification\Common\Phone
{
	use libphonenumber as BasePhoneNumber;
	use Codification\Common\Enum;

	/**
	 * @method static ParseErrorType NONE()
	 * @method static ParseErrorType INVALID_COUNTRY_CODE()
	 * @method static ParseErrorType NOT_A_NUMBER()
	 * @method static ParseErrorType TOO_SHORT_AFTER_IDD()
	 * @method static ParseErrorType TOO_SHORT_NSN()
	 * @method static ParseErrorType TOO_LONG()
	 *
	 * @template-extends \Codification\Common\Enum\Enum<int>
	 */
	final class ParseErrorType extends Enum\Enum
	{
		/** @var int */
		public const NONE = -1;

		/** @var int */
		public const INVALID_COUNTRY_CODE = BasePhoneNumber\NumberParseException::INVALID_COUNTRY_CODE;

		/** @var int */
		public const NOT_A_NUMBER = BasePhoneNumber\NumberParseException::NOT_A_NUMBER;

		/** @var int */
		public const TOO_SHORT_AFTER_IDD = BasePhoneNumber\NumberParseException::TOO_SHORT_AFTER_IDD;

		/** @var int */
		public const TOO_SHORT_NSN = BasePhoneNumber\NumberParseException::TOO_SHORT_NSN;

		/** @var int */
		public const TOO_LONG = BasePhoneNumber\NumberParseException::TOO_LONG;
	}
}