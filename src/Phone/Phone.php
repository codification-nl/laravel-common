<?php

namespace Codification\Common\Phone
{
	use libphonenumber as BasePhoneNumber;
	use Codification\Common\Contracts;
	use Codification\Common\Support;

	final class Phone implements Contracts\Support\Stringable, Support\Contracts\Bindable
	{
		/** @var null|\libphonenumber\PhoneNumber */
		private /*@?BasePhoneNumber\PhoneNumber*/
			$instance = null;

		/**
		 * @param string|null $region_code = null
		 *
		 * @return string
		 *
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function format(string $region_code = null) : string
		{
			$instance = $this->ensureInstance();
			$util     = BasePhoneNumber\PhoneNumberUtil::getInstance();

			if ($region_code === '*')
			{
				return $util->format($instance, BasePhoneNumber\PhoneNumberFormat::E164);
			}

			$region_code = Support\ContainerUtils::resolveLocale($region_code, \CASE_UPPER);

			try
			{
				$result = $util->formatOutOfCountryCallingNumber($instance, $region_code);
			}
			catch (\InvalidArgumentException $e)
			{
				throw new Support\Exceptions\ShouldNotHappenException('Failed to format', $e);
			}

			/** @var string $result */
			$result = \str_replace(' ', '', $result);

			return $result;
		}

		/**
		 * @return string
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function humanize() : string
		{
			return BasePhoneNumber\PhoneNumberUtil::getInstance()->format(
				$this->ensureInstance(),
				BasePhoneNumber\PhoneNumberFormat::NATIONAL
			);
		}

		/**
		 * @param string|null                                     $region_code = null
		 * @param \Codification\Common\Phone\PhoneType|int|string $type        = PhoneType::BOTH
		 *
		 * @return bool
		 *
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function isValid(string $region_code = null, $type = PhoneType::BOTH) : bool
		{
			$instance    = $this->ensureInstance();
			$region_code = Support\ContainerUtils::resolveLocale($region_code, \CASE_UPPER);

			if (!($type instanceof PhoneType))
			{
				/** @var \Codification\Common\Phone\PhoneType $enum */
				$enum = PhoneType::make($type);

				$type = $enum;
			}

			switch (BasePhoneNumber\PhoneNumberUtil::getInstance()->getNumberType($instance))
			{
				case BasePhoneNumber\PhoneNumberType::FIXED_LINE_OR_MOBILE:
					break;

				case BasePhoneNumber\PhoneNumberType::MOBILE:
				{
					if ($type->has(PhoneType::MOBILE()))
					{
						break;
					}

					return false;
				}

				case BasePhoneNumber\PhoneNumberType::FIXED_LINE:
				{
					if ($type->has(PhoneType::FIXED()))
					{
						break;
					}

					return false;
				}

				default:
					return false;
			}

			try
			{
				return BasePhoneNumber\PhoneNumberUtil::getInstance()->isValidNumberForRegion($instance, $region_code);
			}
			catch (\InvalidArgumentException $e)
			{
				throw new Support\Exceptions\ShouldNotHappenException('Failed to validate', $e);
			}
		}

		/**
		 * @param string|null                                     $number
		 * @param string|null                                     $region_code
		 * @param \Codification\Common\Phone\PhoneType|int|string $type            = PhoneType::BOTH
		 * @param \Codification\Common\Phone\ParseErrorType|null  $out_parse_error = null
		 * @param-out \Codification\Common\Phone\ParseErrorType|null $out_parse_error = null
		 *
		 * @return bool
		 *
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 */
		public static function validate(?string $number, ?string $region_code, $type = PhoneType::BOTH, ParseErrorType &$out_parse_error = null) : bool
		{
			$phone = self::make($number, $region_code, $out_parse_error);

			if ($phone === null)
			{
				return false;
			}

			return $phone->isValid($region_code, $type);
		}

		/**
		 * @param string|null                                    $number
		 * @param string|null                                    $region_code
		 * @param \Codification\Common\Phone\ParseErrorType|null $out_parse_error = null
		 * @param-out \Codification\Common\Phone\ParseErrorType|null $out_parse_error = null
		 *
		 * @return \Codification\Common\Phone\Phone|null
		 *
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function make(?string $number, ?string $region_code, ParseErrorType &$out_parse_error = null) : ?Phone
		{
			$number      = \sanitize($number);
			$region_code = \sanitize(\strtoupper($region_code ?? ''));

			try
			{
				$phone = new self();

				/** @psalm-suppress PossiblyNullArgument */
				$phone->instance = BasePhoneNumber\PhoneNumberUtil::getInstance()->parse($number, $region_code);

				return $phone;
			}
			catch (BasePhoneNumber\NumberParseException $e)
			{
				if ($out_parse_error !== null)
				{
					/** @var int $value */
					$value = $e->getCode();

					/** @var \Codification\Common\Phone\ParseErrorType $enum */
					$enum = ParseErrorType::make($value);

					$out_parse_error = $enum;
				}
			}

			return null;
		}

		/**
		 * @param string|null $number
		 * @param string|null $region_code = null
		 *
		 * @return string|null
		 *
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 *
		 * @noinspection BadExceptionsProcessingInspection
		 */
		public static function getRegionCode(?string $number, string $region_code = null) : ?string
		{
			$util        = BasePhoneNumber\PhoneNumberUtil::getInstance();
			$number      = \sanitize($number);
			$region_code = Support\ContainerUtils::resolveLocale($region_code, \CASE_UPPER);

			try
			{
				/** @psalm-suppress PossiblyNullArgument */
				$phone = $util->parse($number, $region_code);

				return $util->getRegionCodeForNumber($phone);
			}
			catch (BasePhoneNumber\NumberParseException $e)
			{
			}

			return null;
		}

		/**
		 * @return string
		 *
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function __toString() : string
		{
			return $this->toString();
		}

		/**
		 * @return string
		 *
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function jsonSerialize() : string
		{
			return $this->format();
		}

		/**
		 * @return string
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function toString() : string
		{
			return $this->format();
		}

		/**
		 * @return \libphonenumber\PhoneNumber
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		private function ensureInstance() : BasePhoneNumber\PhoneNumber
		{
			if ($this->instance === null)
			{
				throw new Support\Exceptions\ShouldNotHappenException('$this->instance === null');
			}

			return $this->instance;
		}
	}
}