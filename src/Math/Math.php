<?php

namespace Codification\Common\Math
{
	use Codification\Common\Support;

	/**
	 * @method static \Codification\Common\Math\Number add(\Codification\Common\Math\Number|string|float|int $lhs, \Codification\Common\Math\Number|string|float|int $rhs, int|null $scale = null)
	 * @method static \Codification\Common\Math\Number sub(\Codification\Common\Math\Number|string|float|int $lhs, \Codification\Common\Math\Number|string|float|int $rhs, int|null $scale = null)
	 * @method static \Codification\Common\Math\Number mul(\Codification\Common\Math\Number|string|float|int $lhs, \Codification\Common\Math\Number|string|float|int $rhs, int|null $scale = null)
	 * @method static \Codification\Common\Math\Number div(\Codification\Common\Math\Number|string|float|int $lhs, \Codification\Common\Math\Number|string|float|int $rhs, int|null $scale = null)
	 * @method static \Codification\Common\Math\Number pow(\Codification\Common\Math\Number|string|float|int $base, string|int $exponent, int|null $scale = null)
	 */
	final class Math implements Support\Contracts\Bindable
	{
		/** @var int */
		public const ROUND_UP = \PHP_ROUND_HALF_UP;

		/** @var int */
		public const ROUND_DOWN = \PHP_ROUND_HALF_DOWN;

		/** @var int */
		public const SIGN_POSITIVE = 1;

		/** @var int */
		public const SIGN_NEGATIVE = -1;

		/** @var int */
		public const SIGN_ZERO = 0;

		/** @var int */
		public const COMP_EQ = 0;

		/** @var int */
		public const COMP_GT = 1;

		/** @var int */
		public const COMP_LT = -1;

		/** @var int */
		public static /*@int*/
			$scale = 8;

		/**
		 * @param string $name
		 * @psalm-param array{0: \Codification\Common\Math\Number|numeric, 1: \Codification\Common\Math\Number|numeric, 2: int|null} $arguments
		 * @param mixed  $arguments
		 *
		 * @return \Codification\Common\Math\Number
		 *
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function __callStatic($name, $arguments)
		{
			[$lhs, $rhs, $scale] = $arguments + [null, null, null];

			if ($scale === null)
			{
				if ($lhs instanceof Number)
				{
					$scale = $lhs->getScale();
				}
				else
				{
					$scale = static::$scale;
				}
			}

			$op    = MathOp::make($name);
			$value = self::bc($op, $lhs, $rhs, $scale);

			return Number::make($value, $scale);
		}

		/**
		 * @psalm-param \Codification\Common\Math\Number|numeric    $value
		 * @param \Codification\Common\Math\Number|string|int|float $value
		 *
		 * @psalm-return Math::SIGN_ZERO|Math::SIGN_NEGATIVE|Math::SIGN_POSITIVE
		 * @return int
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function sign($value) : int
		{
			$result = self::getValue($value);

			if ($result === '0')
			{
				return self::SIGN_ZERO;
			}

			if (self::isNegative($result))
			{
				return self::SIGN_NEGATIVE;
			}

			return self::SIGN_POSITIVE;
		}

		/**
		 * @psalm-param \Codification\Common\Math\Number|numeric    $value
		 * @param \Codification\Common\Math\Number|string|int|float $value
		 * @psalm-param int|numeric-string                          $precision = 0
		 * @param int|string                                        $precision = 0
		 * @psalm-param Math::ROUND_UP|Math::ROUND_DOWN|null        $mode      = null
		 * @param int|null                                          $mode      = null
		 *
		 * @return \Codification\Common\Math\Number
		 *
		 * @throws \DivisionByZeroError
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function round($value, $precision = 0, int $mode = null) : \Codification\Common\Math\Number
		{
			$scale  = null;
			$result = self::getValue($value, $scale);

			switch ($mode)
			{
				case self::ROUND_UP:
				case self::ROUND_DOWN:
				{
					$op = ($mode === self::ROUND_UP) ? 'ceil' : 'floor';

					/** @noinspection ArgumentEqualsDefaultValueInspection */
					$rhs = \bcpow('10', (string)$precision, 0);
					/** @var string $lhs */
					$lhs    = self::{$op}(\bcmul($result, $rhs, self::getDecimals($result)));
					$result = \bcdiv($lhs, $rhs, (int)$precision);

					if ($result === null)
					{
						throw new \DivisionByZeroError('');
					}

					return Number::make($result, $scale);
				}

				default:
				{
					$op = self::isNegative($result) ? MathOp::SUB() : MathOp::ADD();

					$rhs    = '0.' . \str_repeat('0', (int)$precision) . '5';
					$result = self::bc($op, $result, $rhs, $precision);

					return Number::make($result, $scale);
				}
			}
		}

		/**
		 * @psalm-param \Codification\Common\Math\Number|numeric    $value
		 * @param \Codification\Common\Math\Number|string|int|float $value
		 * @param int|null                                          $scale = null
		 *
		 * @return \Codification\Common\Math\Number
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function floor($value, int $scale = null) : \Codification\Common\Math\Number
		{
			$result = self::getValue($value, $scale);

			if (self::hasDecimals($result))
			{
				$rhs = self::isNegative($result) ? '1' : '0';
				/** @noinspection ArgumentEqualsDefaultValueInspection */
				$result = \bcsub($result, $rhs, 0);
			}

			return Number::make($result, $scale);
		}

		/**
		 * @psalm-param \Codification\Common\Math\Number|numeric    $value
		 * @param \Codification\Common\Math\Number|string|int|float $value
		 * @param int|null                                          $scale = null
		 *
		 * @return \Codification\Common\Math\Number
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function ceil($value, int $scale = null) : \Codification\Common\Math\Number
		{
			$result = self::getValue($value, $scale);

			if (self::hasDecimals($result))
			{
				$rhs = self::isNegative($result) ? '0' : '1';
				/** @noinspection ArgumentEqualsDefaultValueInspection */
				$result = \bcadd($result, $rhs, 0);
			}

			return Number::make($result, $scale);
		}

		/**
		 * @psalm-param \Codification\Common\Math\Number|numeric    $value
		 * @param \Codification\Common\Math\Number|string|int|float $value
		 * @param int|null                                          $scale = null
		 *
		 * @return \Codification\Common\Math\Number
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function abs($value, int $scale = null) : \Codification\Common\Math\Number
		{
			$result = self::getValue($value, $scale);

			if (self::isNegative($result))
			{
				$result = \substr($result, 1);

				if ($result === false)
				{
					throw new Support\Exceptions\ShouldNotHappenException('Failed to get part of string');
				}
			}

			return Number::make($result, $scale);
		}

		/**
		 * @psalm-param \Codification\Common\Math\Number|numeric    $value
		 * @param \Codification\Common\Math\Number|string|int|float $value
		 *
		 * @return bool
		 */
		private static function hasDecimals($value) : bool
		{
			return (\strpos((string)$value, '.') !== false);
		}

		/**
		 * @psalm-param \Codification\Common\Math\Number|numeric    $value
		 * @param \Codification\Common\Math\Number|string|int|float $value
		 *
		 * @return bool
		 */
		private static function isNegative($value) : bool
		{
			return (\strncmp('-', (string)$value, 1) === 0);
		}

		/**
		 * @psalm-param \Codification\Common\Math\Number|numeric    $value
		 * @param \Codification\Common\Math\Number|string|int|float $value
		 *
		 * @return int
		 */
		private static function getDecimals($value) : int
		{
			if (!self::hasDecimals($value))
			{
				return 0;
			}

			[, $decimals] = \explode('.', (string)$value, 2);

			return \strlen($decimals);
		}

		/**
		 * @psalm-param \Codification\Common\Math\Number|numeric    $value
		 * @param \Codification\Common\Math\Number|string|int|float $value
		 * @param-out    int|null                                   $scale = -1
		 * @param int|null                                          $scale = -1
		 *
		 * @psalm-return numeric-string
		 * @return string
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		private static function getValue($value, &$scale = -1) : string
		{
			if ($value instanceof Number)
			{
				if ($scale !== -1 && $scale === null)
				{
					$scale = $value->getScale();
				}

				return $value->getValue();
			}

			return self::trim($value);
		}

		/**
		 * @param \Codification\Common\Math\MathOp                  $math_op
		 * @psalm-param \Codification\Common\Math\Number|numeric    ...$params
		 * @param \Codification\Common\Math\Number|string|int|float ...$params
		 *
		 * @psalm-return numeric-string|int
		 * @return string|int - Returns 'trimmed' result.
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function bc(MathOp $math_op, ...$params)
		{
			$op = $math_op->isComparison() ? 'comp' : $math_op->getValue();

			/** @var callable $fn */
			$fn = "bc{$op}";

			/** @psalm-var numeric-string|int|false $value */
			$value = \call_user_func_array($fn, $params);

			if ($value === false)
			{
				throw new Support\Exceptions\ShouldNotHappenException("Failed to call [bc{$op}]");
			}

			if ($math_op->isComparison())
			{
				/** @var int $value */
				return $value;
			}

			return self::trim($value);
		}

		/**
		 * @psalm-param \Codification\Common\Math\Number|numeric    $value
		 * @param \Codification\Common\Math\Number|string|int|float $value
		 *
		 * @psalm-return numeric-string
		 * @return string
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function trim($value) : string
		{
			if ($value instanceof Number)
			{
				return $value->getValue();
			}

			$value = (string)$value;

			/** @var string|false $value */
			$value = \filter_var($value, \FILTER_SANITIZE_NUMBER_FLOAT, \FILTER_FLAG_ALLOW_FRACTION);

			if ($value === false)
			{
				throw new Support\Exceptions\ShouldNotHappenException('Failed to filter string');
			}

			/** @var string $value */
			$value = \str_replace('+', '', $value);

			if (self::hasDecimals($value))
			{
				$value = \rtrim($value, '0');
			}

			$value = \rtrim($value, '.');

			if (!$value || $value === '-0')
			{
				$value = '0';
			}

			/** @psalm-var numeric-string $value */
			return $value;
		}
	}
}