<?php

namespace Codification\Common\Math
{
	use Codification\Common\Contracts;
	use Codification\Common\Support;

	/**
	 * @method \Codification\Common\Math\Number add(\Codification\Common\Math\Number|string|float|int $other)
	 * @method \Codification\Common\Math\Number sub(\Codification\Common\Math\Number|string|float|int $other)
	 * @method \Codification\Common\Math\Number mul(\Codification\Common\Math\Number|string|float|int $other)
	 * @method \Codification\Common\Math\Number div(\Codification\Common\Math\Number|string|float|int $other)
	 * @method bool eq(\Codification\Common\Math\Number|string|float|int $other)
	 * @method bool ne(\Codification\Common\Math\Number|string|float|int $other)
	 * @method bool gt(\Codification\Common\Math\Number|string|float|int $other)
	 * @method bool lt(\Codification\Common\Math\Number|string|float|int $other)
	 * @method bool gte(\Codification\Common\Math\Number|string|float|int $other)
	 * @method bool lte(\Codification\Common\Math\Number|string|float|int $other)
	 */
	final class Number implements \JsonSerializable, Contracts\Support\Cloneable, Contracts\Support\Stringable
	{
		/**
		 * @psalm-var numeric-string
		 * @var string
		 */
		private /*@string*/
			$value;

		/** @var int */
		private /*@int*/
			$scale;

		/**
		 * @psalm-param numeric    $value
		 * @param int|float|string $value
		 * @param int|null         $scale = null
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function __construct($value, int $scale = null)
		{
			$this->setValue($value);
			$this->setScale($scale);
		}

		/**
		 * @psalm-param numeric    $value
		 * @param int|float|string $value
		 * @param int|null         $scale = null
		 * @return \Codification\Common\Math\Number
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function make($value, int $scale = null) : self
		{
			return new self($value, $scale);
		}

		/**
		 * @psalm-return numeric-string
		 * @return string
		 */
		public function getValue() : string
		{
			return $this->value;
		}

		/**
		 * @psalm-param numeric $value
		 * @param int|float|string $value
		 *
		 * @return self
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function setValue($value) : self
		{
			$this->value = Math::trim($value);

			return $this;
		}

		/**
		 * @return int
		 */
		public function getScale() : int
		{
			return $this->scale;
		}

		/**
		 * @param int|null $scale = null
		 *
		 * @return self
		 */
		public function setScale(int $scale = null) : self
		{
			$this->scale = $scale ?? Math::$scale;

			return $this;
		}

		/**
		 * @param string                       $name
		 * @psalm-param list<self|numeric>     $arguments
		 * @param array<self|string|float|int> $arguments
		 *
		 * @return self|bool
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 */
		public function __call($name, $arguments)
		{
			[$other] = $arguments;

			$math_op = MathOp::make($name);
			$value   = Math::bc($math_op, $this, $other, $this->scale);

			if ($math_op->isComparison())
			{
				/** @var int $value */
				return self::compare($math_op, $value);
			}

			/** @var string $value */
			return new self($value, $this->scale);
		}

		/**
		 * @param \Codification\Common\Math\MathOp $name
		 * @param int                              $value
		 *
		 * @return bool
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		private static function compare(MathOp $name, int $value) : bool
		{
			switch ($name->getValue())
			{
				case MathOp::EQ:
					return ($value === Math::COMP_EQ);

				case MathOp::NE:
					return ($value !== Math::COMP_EQ);

				case MathOp::GT:
					return ($value === Math::COMP_GT);

				case MathOp::LT:
					return ($value === Math::COMP_LT);

				case MathOp::GTE:
					return ($value === Math::COMP_GT || $value === Math::COMP_EQ);

				case MathOp::LTE:
					return ($value === Math::COMP_LT || $value === Math::COMP_EQ);

				default:
					throw new Support\Exceptions\ShouldNotHappenException("Unknown operation [{$name}]");
			}
		}

		/**
		 * @return int
		 */
		public function toInt() : int
		{
			return (int)$this->value;
		}

		/**
		 * @return float
		 */
		public function toFloat() : float
		{
			return (float)$this->value;
		}

		/**
		 * @psalm-return numeric-string
		 * @return string
		 */
		public function toString() : string
		{
			return $this->value;
		}

		/**
		 * @return self
		 */
		public function copy() : self
		{
			return clone $this;
		}

		/**
		 * @return self
		 */
		public function clone() : self
		{
			return clone $this;
		}

		/**
		 * @psalm-return numeric-string
		 * @return string
		 */
		public function jsonSerialize() : string
		{
			return $this->getValue();
		}

		/**
		 * @psalm-return numeric-string
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->toString();
		}

		/**
		 * @return self
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function __clone()
		{
			return new self($this->value, $this->scale);
		}
	}
}