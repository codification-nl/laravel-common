<?php

namespace Codification\Common\Math
{
	use Codification\Common\Enum;

	/**
	 * @method static MathOp ADD()
	 * @method static MathOp SUB()
	 * @method static MathOp MUL()
	 * @method static MathOp DIV()
	 * @method static MathOp COMP()
	 * @method static MathOp EQ()
	 * @method static MathOp NE()
	 * @method static MathOp GT()
	 * @method static MathOp LT()
	 * @method static MathOp GTE()
	 * @method static MathOp LTE()
	 *
	 * @template-extends \Codification\Common\Enum\Enum<string>
	 */
	final class MathOp extends Enum\Enum
	{
		/** @var string */
		public const ADD = 'add';

		/** @var string */
		public const SUB = 'sub';

		/** @var string */
		public const MUL = 'mul';

		/** @var string */
		public const DIV = 'div';

		/** @var string */
		public const COMP = 'comp';

		/** @var string */
		public const EQ = 'eq';

		/** @var string */
		public const NE = 'ne';

		/** @var string */
		public const GT = 'gt';

		/** @var string */
		public const LT = 'lt';

		/** @var string */
		public const GTE = 'gte';

		/** @var string */
		public const LTE = 'lte';

		/**
		 * @return bool
		 */
		public function isComparison() : bool
		{
			switch ($this->getValue())
			{
				case self::EQ:
				case self::NE:
				case self::GT:
				case self::LT:
				case self::GTE:
				case self::LTE:
					return true;

				default:
					return false;
			}
		}
	}
}