<?php

namespace Codification\Common\Mail\Exceptions
{
	final class SocketException extends \RuntimeException
	{
		/**
		 * @param string $connection
		 * @param int    $error_code
		 * @param string $error_message
		 */
		public function __construct(string $connection, int $error_code, string $error_message)
		{
			parent::__construct("[{$connection}] Failed to open: {$error_message} ({$error_code})");
		}
	}
}