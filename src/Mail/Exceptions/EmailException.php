<?php

namespace Codification\Common\Mail\Exceptions
{
	final class EmailException extends \UnexpectedValueException
	{
		/**
		 * @param string|null $address = null
		 */
		public function __construct(string $address = null)
		{
			if ($address === null)
			{
				$address = 'null';
			}

			parent::__construct("[{$address}] is invalid");
		}
	}
}