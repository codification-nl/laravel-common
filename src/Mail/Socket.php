<?php

namespace Codification\Common\Mail
{
	use Codification\Common\Support;

	final class Socket
	{
		/** @var null|string */
		private /*@?string*/
			$connection;

		/** @var null|resource */
		private $socket;

		/**
		 * @psalm-var null|\Closure(string,array):void
		 * @var null|\Closure
		 */
		public static /*@?\Closure*/
			$logger;

		/**
		 * @param null|\Closure $fn
		 * @psalm-param null|\Closure(string,array):void $fn
		 * @return void
		 */
		public static function setLogger(?\Closure $fn) : void
		{
			self::$logger = $fn;
		}

		/**
		 * @param string $hostname
		 * @param int    $port
		 * @param int    $timeout
		 *
		 * @throws \Codification\Common\Mail\Exceptions\SocketException
		 */
		public function __construct(string $hostname, int $port, int $timeout)
		{
			$this->connection = "{$hostname}:{$port}";

			$error_code    = -1;
			$error_message = '';

			/** @noinspection PhpUsageOfSilenceOperatorInspection */
			$socket = @\fsockopen($hostname, $port, $error_code, $error_message, $timeout);

			if ($socket === false)
			{
				throw new Exceptions\SocketException($this->connection, $error_code, $error_message);
			}

			$this->socket = $socket;
		}

		/**
		 * @psalm-param 'open'|'write'|'read' $command
		 * @param string       $command
		 * @param string|false $value
		 * @param array        $context = []
		 *
		 * @return void
		 */
		private function log(string $command, $value, array $context = []) : void
		{
			$fn = self::$logger;

			if ($fn === null)
			{
				return;
			}

			$value = \json_encode($value, \JSON_THROW_ON_ERROR);

			$fn("[{$this->connection}] {$command}: {$value}", $context);
		}

		/**
		 * @param string   $value
		 * @param int|null $expect = null
		 *
		 * @return bool
		 *
		 * @throws \Codification\Common\Support\Exceptions\ReferenceException
		 */
		public function command(string $value, int $expect = null) : bool
		{
			if ($this->socket === null)
			{
				throw new Support\Exceptions\ReferenceException('$this->socket');
			}

			$this->log('write', $value);

			if (\fwrite($this->socket, $value) === false)
			{
				return false;
			}

			if ($expect !== null)
			{
				return $this->read($expect);
			}

			return true;
		}

		/**
		 * @param int $expect
		 *
		 * @return bool
		 *
		 * @throws \Codification\Common\Support\Exceptions\ReferenceException
		 */
		public function read(int $expect) : bool
		{
			if ($this->socket === null)
			{
				throw new Support\Exceptions\ReferenceException('$this->socket');
			}

			$result = \fgets($this->socket, 1024);

			$this->log('read', $result, [
				'expect' => $expect,
			]);

			if ($result === false)
			{
				return false;
			}

			return ((int)\substr($result, 0, 3) === $expect);
		}

		/**
		 * @return bool
		 */
		public function close() : bool
		{
			if ($this->socket === null)
			{
				return true;
			}

			$result = \fclose($this->socket);

			$this->socket = null;

			return $result;
		}

		/**
		 * Socket destructor.
		 */
		public function __destruct()
		{
			$this->close();
		}
	}
}