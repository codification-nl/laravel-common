<?php

namespace Codification\Common\Mail
{
	use Codification\Common\Support;

	final class Email implements Support\Contracts\Bindable
	{
		/** @var int */
		public const SMTP_READY = 220;

		/** @var int */
		public const SMTP_COMPLETED = 250;

		/** @var int */
		public const SMTP_PORT = 25;

		/** @var int */
		public const SMTP_TIMEOUT = 5;

		/** @var null|string */
		private static /*@?string*/
			$from;

		/**
		 * @var null|\Closure
		 * @psalm-var null|\Closure(string,\Closure():bool):bool
		 */
		private static /*@?\Closure*/
			$remember;

		/**
		 * @param null|string $address
		 * @return void
		 */
		public static function from(?string $address) : void
		{
			self::$from = $address;
		}

		/**
		 * @param null|\Closure $fn
		 * @psalm-param null|\Closure(string,\Closure():bool):bool $fn
		 * @return void
		 */
		public static function cache(?\Closure $fn) : void
		{
			self::$remember = $fn;
		}

		/**
		 * @param null|\Closure $fn
		 * @psalm-param null|\Closure(string,array):void $fn
		 * @return void
		 */
		public static function logger(?\Closure $fn) : void
		{
			Socket::setLogger($fn);
		}

		/**
		 * @param string|null $address
		 * @param string|null $from    = null
		 * @param int         $timeout = Mail::SMTP_TIMEOUT
		 *
		 * @return bool
		 *
		 * @throws \Codification\Common\Mail\Exceptions\EmailException
		 * @throws \Codification\Common\Support\Exceptions\ReferenceException
		 * @throws \Codification\Common\Support\Exceptions\ResolutionException
		 *
		 * @noinspection BadExceptionsProcessingInspection
		 */
		public static function isValid(?string $address, string $from = null, int $timeout = Email::SMTP_TIMEOUT) : bool
		{
			if ($from === null)
			{
				$from = self::$from;
			}

			if ($from === null)
			{
				/** @var \Illuminate\Config\Repository $config */
				$config = Support\ContainerUtils::resolve('config');

				/** @var string|null $from */
				$from = $config->get('mail.from.address');
			}

			$from_value = self::sanitize($from);

			if ($from_value === null)
			{
				throw new Exceptions\EmailException($from);
			}

			$from = $from_value;
			$to   = self::sanitize($address);

			if ($to === null)
			{
				return false;
			}

			try
			{
				$remember = self::$remember;

				if ($remember === null)
				{
					return self::smtp($from, $to, $timeout);
				}

				/*@return $remember("{$from}:{$to}", static fn() : bool => self::smtp($from, $to, $timeout));*/

				return $remember("{$from}:{$to}", static function () use ($from, $to, $timeout) : bool
					{
						return self::smtp($from, $to, $timeout);
					});
			}
			catch (Exceptions\SocketException $e)
			{
				return false;
			}
		}

		/**
		 * @param string|null $address
		 * @return string|null
		 */
		public static function sanitize(?string $address) : ?string
		{
			return \sanitize(\sanitize($address, \FILTER_SANITIZE_EMAIL), \FILTER_VALIDATE_EMAIL);
		}

		/**
		 * @param string $from
		 * @param string $to
		 * @param int    $timeout
		 * @return bool
		 * @throws \Codification\Common\Support\Exceptions\ReferenceException
		 * @throws \Codification\Common\Mail\Exceptions\SocketException
		 */
		private static function smtp(string $from, string $to, int $timeout) : bool
		{
			/** @var string $hostname */
			[, $hostname] = \explode('@', $to, 2);

			/**
			 * @var string[] $hosts
			 * @psalm-var list<string> $hosts
			 */
			$hosts = [];

			/**
			 * @var int[] $weights
			 * @psalm-var list<int> $weights
			 */
			$weights = [];

			\getmxrr($hostname, $hosts, $weights);

			if (empty($hosts))
			{
				return false;
			}

			\arsort($weights, \SORT_NUMERIC);

			/** @var string $hostname */
			$hostname = $hosts[\array_keys($weights)[0] ?? 0];

			return self::socket($hostname, $timeout, static function (Socket $socket) use ($hostname, $from, $to) : bool
				{
					if (!$socket->command("HELO $hostname\r\n", self::SMTP_COMPLETED))
					{
						return false;
					}

					if (!$socket->command("MAIL FROM: <$from>\r\n", self::SMTP_COMPLETED))
					{
						return false;
					}

					if (!$socket->command("RCPT TO: <$to>\r\n", self::SMTP_COMPLETED))
					{
						return false;
					}

					$socket->command('QUIT');

					return true;
				}
			);
		}

		/**
		 * @param string   $hostname
		 * @param int      $timeout
		 * @param \Closure $fn
		 * @psalm-param \Closure(\Codification\Common\Mail\Socket):bool $fn
		 * @return bool
		 * @throws \Codification\Common\Mail\Exceptions\SocketException
		 * @throws \Codification\Common\Support\Exceptions\ReferenceException
		 */
		private static function socket(string $hostname, int $timeout, \Closure $fn) : bool
		{
			$socket = new Socket($hostname, self::SMTP_PORT, $timeout);

			$result = $socket->read(self::SMTP_READY);

			if ($result)
			{
				$result = $fn($socket);
			}

			$socket->close();

			return $result;
		}
	}
}