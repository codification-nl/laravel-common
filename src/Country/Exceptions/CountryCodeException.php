<?php

namespace Codification\Common\Country\Exceptions
{
	final class CountryCodeException extends \DomainException
	{
		/**
		 * @param string|null $code = null
		 */
		public function __construct(string $code = null)
		{
			if ($code === null)
			{
				$code = 'null';
			}

			parent::__construct("[{$code}] is invalid");
		}
	}
}