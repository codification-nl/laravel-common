<?php

namespace Codification\Common\Country
{
	use League\ISO3166;
	use Codification\Common\Support;

	final class Country implements Support\Contracts\Bindable
	{
		/**
		 * @param string|null $country_code
		 *
		 * @return bool
		 *
		 * @noinspection BadExceptionsProcessingInspection
		 */
		public static function isValid(?string $country_code) : bool
		{
			$country_code = \sanitize($country_code);

			if ($country_code === null)
			{
				return false;
			}

			try
			{
				(new ISO3166\ISO3166())->alpha2($country_code);
			}
			catch (\Exception $e)
			{
				return false;
			}

			return true;
		}

		/**
		 * @param string|null $country_code
		 *
		 * @return void
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 */
		public static function ensureValid(?string $country_code) : void
		{
			if (self::isValid($country_code))
			{
				return;
			}

			throw new Exceptions\CountryCodeException($country_code);
		}
	}
}