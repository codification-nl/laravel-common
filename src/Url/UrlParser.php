<?php

/** @noinspection RegExpUnexpectedAnchor */

namespace Codification\Common\Url
{
	use Illuminate;
	use Codification\Common\Contracts;
	use Codification\Common\Support;

	final class UrlParser implements Contracts\Support\Stringable, Support\Contracts\Bindable
	{
		/** @var string */
		public const SCHEME_PATTERN = /** @lang RegExp */
			'/^https?:\/\//';

		/** @var string */
		public const HOST_PATTERN = /** @lang RegExp */
			'/^([a-zA-Z]+:\/\/)?www\./';

		/** @var string */
		public /*@string*/
			$scheme = 'https';

		/** @var string */
		public /*@string*/
			$host = 'localhost';

		/** @var null|int */
		public /*@?int*/
			$port = null;

		/** @var string */
		public /*@string*/
			$path = '';

		/** @var null|\Codification\Common\Url\UrlQueryParams */
		public /*@?UrlQueryParams*/
			$query = null;

		/** @var null|string */
		public /*@?string*/
			$fragment = null;

		/**
		 * @param string|null                                $url
		 * @param \Codification\Common\Url\UrlParseFlags|int $flags = UrlParseFlags::ALL
		 *
		 * @return \Codification\Common\Url\UrlParser
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Url\Exceptions\InvalidUrlException
		 */
		public static function parse(?string $url, $flags = UrlParseFlags::ALL) : self
		{
			$url_value = self::sanitize($url, $flags);

			if ($url_value === null)
			{
				throw new Exceptions\InvalidUrlException($url);
			}

			/**
			 * @psalm-var array{scheme: string|null, host: string|null, port: int|null, path: string|null, query: string|null, fragment: string|null} $parsed
			 * @var array<string, string|null> $parsed
			 */
			$parsed = \parse_url($url_value);

			$result = new self();

			/** @psalm-var list<string> $parts */
			$parts = UrlPart::values();

			foreach ($parts as $part)
			{
				switch ($part)
				{
					case UrlPart::HOST:
						$result->host = $parsed[UrlPart::HOST] ?? '';
						break;

					case UrlPart::PATH:
						$result->path = $parsed[UrlPart::PATH] ?? '/';
						break;

					case  UrlPart::QUERY:
						$result->query = UrlQueryParams::parse($parsed[UrlPart::QUERY] ?? '');
						break;

					default:
						$result->{$part} = $parsed[$part] ?? null;
						break;
				}
			}

			return $result;
		}

		/**
		 * @param string|null                                $url
		 * @param \Codification\Common\Url\UrlParseFlags|int $flags = UrlParseFlags::ALL
		 *
		 * @return string
		 *
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function sanitize(?string $url, $flags = UrlParseFlags::ALL) : ?string
		{
			$url = \sanitize($url, \FILTER_SANITIZE_URL);

			if ($url === null)
			{
				return null;
			}

			if (!($flags instanceof UrlParseFlags))
			{
				/** @var \Codification\Common\Url\UrlParseFlags $enum */
				$enum = UrlParseFlags::make($flags);

				$flags = $enum;
			}

			if (\preg_match(self::SCHEME_PATTERN, $url) !== 1)
			{
				$scheme = $flags->has(UrlParseFlags::SECURE()) ? 'https://' : 'http://';
				$url    = $scheme . $url;
			}

			if ($flags->has(UrlParseFlags::STRIP_WWW()))
			{
				/**
				 * @psalm-var array{1: string} $matches
				 * @var string[] $matches
				 */
				$matches = [];

				if (\preg_match(self::HOST_PATTERN, $url, $matches) === 1)
				{
					/** @var string $scheme */
					[, $scheme] = $matches + [null, ''];

					/** @var string|null $url */
					$url = \preg_replace(self::HOST_PATTERN, $scheme, $url);

					if ($url === null)
					{
						throw new Support\Exceptions\ShouldNotHappenException('Failed to replace');
					}
				}
			}

			return $url;
		}

		/**
		 * @param string|array<string> $path
		 *
		 * @return $this
		 */
		public function path($path) : self
		{
			$this->path = Illuminate\Support\Str::start(\implode('/', \is_array($path) ? $path : \func_get_args()), '/');

			return $this;
		}

		/**
		 * @param string $sub_domain
		 *
		 * @return $this
		 */
		public function subDomain(string $sub_domain) : self
		{
			$this->host = "{$sub_domain}.{$this->host}";

			return $this;
		}

		/**
		 * @return string
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function humanize() : string
		{
			if (\strncmp($this->host, 'www.', 4) === 0)
			{
				$host = \substr($this->host, 4);

				if ($host === false)
				{
					throw new Support\Exceptions\ShouldNotHappenException('Failed to get part of string');
				}

				return $host;
			}

			return $this->host;
		}

		/**
		 * @return string
		 */
		public function toString() : string
		{
			$result = "{$this->scheme}://{$this->host}";

			if ($this->port !== null)
			{
				$result .= ":{$this->port}";
			}

			$result .= $this->path;

			if ($this->query !== null && \count($this->query))
			{
				$result .= "?{$this->query->toString()}";
			}

			if ($this->fragment !== null)
			{
				$result .= "#{$this->fragment}";
			}

			return $result;
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->toString();
		}

		/**
		 * @return string
		 */
		public function jsonSerialize() : string
		{
			return $this->toString();
		}
	}
}