<?php

namespace Codification\Common\Url\Exceptions
{
	final class InvalidUrlException extends \InvalidArgumentException
	{
		/**
		 * @param string|null $url = null
		 */
		public function __construct(string $url = null)
		{
			parent::__construct("Invalid url '{$url}'");
		}
	}
}