<?php

namespace Codification\Common\Url
{
	use Codification\Common\Contracts;
	use Codification\Common\Support;

	/**
	 * @template-extends \Codification\Common\Support\Dictionary<array-key, mixed>
	 */
	final class UrlQueryParams extends Support\Dictionary implements Contracts\Support\Stringable
	{
		/**
		 * @param array<array-key, mixed> $parameters
		 *
		 * @return \Codification\Common\Url\UrlQueryParams
		 */
		public static function make(array $parameters) : self
		{
			return new self($parameters);
		}

		/**
		 * @param string $query
		 *
		 * @return \Codification\Common\Url\UrlQueryParams
		 */
		public static function parse(string $query) : self
		{
			$parameters = [];

			\parse_str($query, $parameters);

			return new self($parameters);
		}

		/**
		 * @return string
		 */
		public function toString() : string
		{
			return \http_build_query($this->items);
		}

		/**
		 * @return string
		 */
		public function __toString() : string
		{
			return $this->toString();
		}
	}
}