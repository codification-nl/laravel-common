<?php

namespace Codification\Common\Log
{
	use Codification\Common\Enum;

	/**
	 * @method static LogAction CREATED()
	 * @method static LogAction UPDATED()
	 * @method static LogAction RESTORED()
	 * @method static LogAction DELETED()
	 *
	 * @template-extends \Codification\Common\Enum\Enum<string>
	 */
	final class LogAction extends Enum\Enum
	{
		/** @var string */
		public const CREATED = 'created';

		/** @var string */
		public const UPDATED = 'updated';

		/** @var string */
		public const RESTORED = 'restored';

		/** @var string */
		public const DELETED = 'deleted';
	}
}