<?php

namespace Codification\Common\Log
{
	use Carbon;
	use Illuminate;
	use Codification\Common\Contracts;
	use Codification\Common\Support;

	final class Logger implements Support\Contracts\Bindable
	{
		/** @var string */
		private const ACTION_PATTERN = /**@lang RegExp */
			'/\.(\w+):/';

		/**
		 * @psalm-var list<\Codification\Common\Log\LogEntry>
		 * @var \Codification\Common\Log\LogEntry[]
		 */
		private static /*@array*/
			$entries = [];

		/** @var \Illuminate\Events\Dispatcher */
		private static $events;

		/** @var \Illuminate\Database\DatabaseManager */
		private static $db;

		/** @var \Illuminate\Contracts\Auth\Guard */
		private static $auth;

		/**
		 * @psalm-var null|class-string<\Illuminate\Database\Eloquent\Model>
		 * @var null|string|\Illuminate\Database\Eloquent\Model
		 */
		private static /*@?string*/
			$userModel;

		/**
		 * @psalm-var class-string<\Codification\Common\Log\LogEntry>
		 * @var string|\Codification\Common\Log\LogEntry
		 */
		private static /*@string*/
			$entryModel = LogEntry::class;

		/** @var int */
		public static /*@int*/
			$chunkSize = 100;

		/**
		 * @psalm-param class-string<\Illuminate\Database\Eloquent\Model> $model
		 * @param string|\Illuminate\Database\Eloquent\Model $model
		 *
		 * @return void
		 */
		public static function useUserModel(string $model) : void
		{
			self::$userModel = $model;
		}

		/**
		 * @psalm-param class-string<\Codification\Common\Log\LogEntry> $model
		 * @param string|\Codification\Common\Log\LogEntry $model
		 *
		 * @return void
		 *
		 * @throws \Codification\Common\Log\Exceptions\ModelException
		 */
		public static function useEntryModel(string $model) : void
		{
			if (!\is_subclass_of($model, LogEntry::class))
			{
				throw new Exceptions\ModelException($model);
			}

			self::$entryModel = $model;
		}

		/**
		 * @psalm-return class-string<\Illuminate\Database\Eloquent\Model>
		 * @return string|\Illuminate\Database\Eloquent\Model
		 *
		 * @throws \Codification\Common\Support\Exceptions\ReferenceException
		 */
		public static function getUserModel() : string
		{
			if (self::$userModel !== null)
			{
				return self::$userModel;
			}

			$class    = self::class;
			$property = '$userModel';

			throw new Support\Exceptions\ReferenceException("{$class}::{$property}");
		}

		/**
		 * @return void
		 *
		 * @throws \Codification\Common\Support\Exceptions\ResolutionException
		 */
		public static function enable() : void
		{
			self::resolveContainers();
			self::startRecording();
			self::storeEntriesBeforeTermination();
		}

		/**
		 * @return void
		 *
		 * @throws \Codification\Common\Support\Exceptions\ResolutionException
		 */
		private static function resolveContainers() : void
		{
			/** @var \Illuminate\Events\Dispatcher $events */
			$events = Support\ContainerUtils::resolve('events');

			/** @var \Illuminate\Database\DatabaseManager $db */
			$db = Support\ContainerUtils::resolve('db');

			/** @var \Illuminate\Contracts\Auth\Guard $auth */
			$auth = Support\ContainerUtils::resolve('auth');

			self::$events = $events;
			self::$db     = $db;
			self::$auth   = $auth;
		}

		/**
		 * @return void
		 */
		private static function startRecording() : void
		{
			self::$events->listen('eloquent.*', [self::class, 'recordAction']);
		}

		/**
		 * @return void
		 */
		private static function storeEntriesBeforeTermination() : void
		{
			/*@Support\ContainerUtils::app()->terminating(static fn() => self::store());*/
			Support\ContainerUtils::app()->terminating(static function () : void
				{
					self::store();
				});
		}

		/**
		 * @return void
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		private static function store() : void
		{
			$batch_id = LogHelpers::uuid()->toString();

			$entry = self::$entryModel::dummy();
			$query = self::$db->connection($entry->getConnectionName())->table($entry->getTable());

			$chunked = \array_chunk(self::$entries, self::$chunkSize);

			foreach ($chunked as $chunk)
			{
				/*@$query->insert(\array_map(static fn(LogEntry $log) : array => \array_merge([
					'uuid'       => LogHelpers::uuid()->toString(),
					'batch_id'   => $batch_id,
					'created_at' => Carbon\Carbon::now(),
				], $log->getAttributes()), $chunk));*/
				$query->insert(\array_map(static function (LogEntry $log) use ($batch_id) : array
					{
						return \array_merge([
							'uuid'       => LogHelpers::uuid()->toString(),
							'batch_id'   => $batch_id,
							'created_at' => Carbon\Carbon::now(),
						], $log->getAttributes());
					}, $chunk));
			}
		}

		/**
		 * @param string $event
		 * @psalm-param array{0: \Illuminate\Database\Eloquent\Model} $data
		 * @param array  $data
		 *
		 * @return void
		 *
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \Codification\Common\Support\Exceptions\ReferenceException
		 */
		public static function recordAction(string $event, array $data) : void
		{
			$action = self::action($event);

			/** @var \Illuminate\Database\Eloquent\Model $model */
			[$model] = $data;

			if (self::shouldRecord($action, $model))
			{
				self::record(LogAction::make($action), $model);
			}
		}

		/**
		 * @param \Codification\Common\Log\LogAction  $action
		 * @param \Illuminate\Database\Eloquent\Model $target
		 *
		 * @return array
		 */
		private static function getChanges(LogAction $action, Illuminate\Database\Eloquent\Model $target) : array
		{
			/** @noinspection DegradedSwitchInspection */
			switch ($action->getValue())
			{
				case LogAction::CREATED:
					return $target->getAttributes();

				default:
					return $target->getChanges();
			}
		}

		/**
		 * @param \Codification\Common\Log\LogAction  $action
		 * @param \Illuminate\Database\Eloquent\Model $target
		 *
		 * @return void
		 *
		 * @throws \Codification\Common\Support\Exceptions\ReferenceException
		 */
		private static function record(LogAction $action, Illuminate\Database\Eloquent\Model $target) : void
		{
			$changes = self::getChanges($action, $target);

			if (\method_exists($target, 'mapLogChanges'))
			{
				/** @var array $changes */
				$changes = $target->mapLogChanges($changes);
			}

			/** @var \Codification\Common\Log\LogEntry $entry */
			$entry = new self::$entryModel(\compact('action', 'changes'));

			/** @var \Illuminate\Database\Eloquent\Model|null $user */
			$user = self::$auth->user();

			if ($user !== null)
			{
				$entry->user()->associate($user);
			}

			$entry->loggable()->associate($target);

			self::$entries[] = $entry;
		}

		/**
		 * @param string                              $action
		 * @param \Illuminate\Database\Eloquent\Model $model
		 *
		 * @return bool
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		private static function shouldRecord(string $action, Illuminate\Database\Eloquent\Model $model) : bool
		{
			return LogAction::isValid($action) && self::shouldRecordModel(\get_class($model));
		}

		/**
		 * @psalm-param class-string<\Illuminate\Database\Eloquent\Model> $model
		 * @param string|\Illuminate\Database\Eloquent\Model $model
		 *
		 * @return bool
		 */
		private static function shouldRecordModel(string $model) : bool
		{
			$loggable   = Contracts\Log\Loggable::class;
			$interfaces = \class_interfaces($model);

			return \in_array($loggable, $interfaces, true);
		}

		/**
		 * @param string $event
		 *
		 * @return string
		 */
		private static function action(string $event) : string
		{
			/**
			 * @psalm-var list<string> $matches
			 * @var string[] $matches
			 */
			$matches = [];

			\preg_match(self::ACTION_PATTERN, $event, $matches);

			/** @var string $action */
			[, $action] = $matches;

			return $action;
		}

	}
}