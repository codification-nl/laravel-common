<?php

namespace Codification\Common\Log\Exceptions
{
	use Codification\Common\Log;

	final class ModelException extends \RuntimeException
	{
		/**
		 * @psalm-param class-string<\Illuminate\Database\Eloquent\Model> $model
		 * @param string|\Illuminate\Database\Eloquent\Model $model
		 */
		public function __construct(string $model)
		{
			$entry = Log\LogEntry::class;

			parent::__construct("[{$model}] is not a subclass of [{$entry}]");
		}
	}
}