<?php

namespace Codification\Common\Log
{
	use Ramsey\Uuid;
	use Codification\Common\Contracts;
	use Codification\Common\Support;

	final class LogHelpers
	{
		/**
		 * @return \Ramsey\Uuid\UuidInterface
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 *
		 * @noinspection BadExceptionsProcessingInspection
		 */
		public static function uuid() : Uuid\UuidInterface
		{
			$factory = new Uuid\UuidFactory();

			$factory->setRandomGenerator(new Uuid\Generator\CombGenerator(
				$factory->getRandomGenerator(),
				$factory->getNumberConverter()
			));

			$factory->setCodec(new Uuid\Codec\TimestampFirstCombCodec(
				$factory->getUuidBuilder()
			));

			try
			{
				return $factory->uuid4();
			}
			catch (\Throwable $e)
			{
				throw new Support\Exceptions\ShouldNotHappenException('Failed to generate UUID');
			}
		}
	}
}