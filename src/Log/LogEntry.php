<?php

namespace Codification\Common\Log
{
	use Illuminate;
	use Ramsey\Uuid;
	use Codification\Common\Database;

	/**
	 * @template-implements \Codification\Common\Database\Eloquent\Contracts\Tokenable<\Ramsey\Uuid\UuidInterface>
	 * @psalm-suppress PropertyNotSetInConstructor
	 *
	 * @todo           Convert to trait
	 */
	class LogEntry extends Database\Eloquent\Model implements Database\Eloquent\Contracts\Tokenable, Database\Eloquent\Contracts\HasEnums
	{
		/** @template-use \Codification\Common\Database\Eloquent\HasToken<\Ramsey\Uuid\UuidInterface> */
		use \Codification\Common\Database\Eloquent\HasToken;
		use \Codification\Common\Database\Eloquent\HasEnums;

		/** @var int */
		public const TOKEN_LENGTH = 36;

		/** @var string|null */
		public const UPDATED_AT = null;

		/** @var string */
		public /*@string*/
			$tokenKey = 'uuid';

		/**
		 * @psalm-var list<string>
		 * @var string[]
		 */
		protected $fillable = [
			'action',
			'changes',
		];

		/** @var array<string, string> */
		protected $casts = [
			'changes' => 'json',
		];

		/**
		 * @psalm-var array<string, class-string<\Codification\Common\Enum\Enum<array-key>>>
		 * @var array<string, string|\Codification\Common\Enum\Enum>
		 */
		public /*@array*/
			$enums = [
			'action' => LogAction::class,
		];

		/**
		 * @return string
		 */
		public function getRouteKeyName() : string
		{
			return $this->getTokenKey();
		}

		/**
		 * @param int $length
		 *
		 * @return \Ramsey\Uuid\UuidInterface
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function generateToken(int $length) : Uuid\UuidInterface
		{
			return LogHelpers::uuid();
		}

		/**
		 * @param string $value
		 *
		 * @return \Ramsey\Uuid\UuidInterface
		 *
		 * @throws \Ramsey\Uuid\Exception\InvalidUuidStringException
		 */
		public function getUuidAttribute(string $value) : Uuid\UuidInterface
		{
			return Uuid\Uuid::fromString($value);
		}

		/**
		 * @param string $value
		 *
		 * @return \Ramsey\Uuid\UuidInterface
		 *
		 * @throws \Ramsey\Uuid\Exception\InvalidUuidStringException
		 */
		public function getBatchIdAttribute(string $value) : Uuid\UuidInterface
		{
			return Uuid\Uuid::fromString($value);
		}

		/**
		 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
		 * @throws \Codification\Common\Support\Exceptions\ReferenceException
		 */
		public function user() : Illuminate\Database\Eloquent\Relations\BelongsTo
		{
			return $this->belongsTo(
				Logger::getUserModel(),
				'user_id',
				'id',
				'user'
			);
		}

		/**
		 * @return \Illuminate\Database\Eloquent\Relations\MorphTo
		 */
		public function loggable() : Illuminate\Database\Eloquent\Relations\MorphTo
		{
			return $this->morphTo(
				'loggable',
				'loggable_type',
				'loggable_id',
				'id'
			);
		}
	}
}