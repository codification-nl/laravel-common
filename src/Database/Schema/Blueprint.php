<?php

namespace Codification\Common\Database\Schema
{
	use Illuminate;
	use Codification\Common\Database;
	use Codification\Common\Support;

	/**
	 * @template       T of \Illuminate\Database\Eloquent\Model
	 * @psalm-suppress PropertyNotSetInConstructor
	 */
	class Blueprint extends \Illuminate\Database\Schema\Blueprint
	{
		/**
		 * @var string|\Illuminate\Database\Eloquent\Model|null
		 * @psalm-var class-string<T>|null
		 */
		protected /*@?string*/
			$model;

		/**
		 * @var \Illuminate\Database\Eloquent\Model|null
		 * @psalm-var T|null
		 */
		protected /*@?Illuminate\Database\Eloquent\Model*/
			$instance;

		/**
		 * @param string                                          $table
		 * @param \Closure|null                                   $callback = null
		 * @psalm-param null|\Closure(\Codification\Common\Database\Schema\Blueprint):void $callback = null
		 * @param string                                          $prefix   = ''
		 * @param string|\Illuminate\Database\Eloquent\Model|null $model    = null
		 * @psalm-param class-string<T>|null                      $model    = null
		 * @param \Illuminate\Database\Eloquent\Model|null        $instance = null
		 * @psalm-param T|null                                    $instance = null
		 */
		public function __construct(string $table, \Closure $callback = null, string $prefix = '', string $model = null, Illuminate\Database\Eloquent\Model $instance = null)
		{
			$this->model    = $model;
			$this->instance = $instance;

			parent::__construct($table, $callback, $prefix);
		}

		/**
		 * @return \Illuminate\Database\Eloquent\Model
		 * @psalm-return T
		 * @throws \Codification\Common\Support\Exceptions\ReferenceException
		 */
		private function ensureInstance() : Illuminate\Database\Eloquent\Model
		{
			if ($this->instance === null)
			{
				throw new Support\Exceptions\ReferenceException('$this->instance');
			}

			return $this->instance;
		}

		/**
		 * @param string|null $column = null
		 * @param int|null    $length = 60
		 *
		 * @return \Illuminate\Database\Schema\ColumnDefinition
		 * @throws \Codification\Common\Support\Exceptions\ReferenceException
		 * @throws \Codification\Common\Support\Exceptions\TraitNotFoundException
		 */
		public function token(string $column = null, int $length = null) : Illuminate\Database\Schema\ColumnDefinition
		{
			if ($column === null)
			{
				/** @var \Codification\Common\Database\Eloquent\Contracts\Tokenable $tokenable */
				$tokenable = $this->ensureInstance();

				if (!\in_array(Database\Eloquent\HasToken::class, \class_traits($tokenable), true))
				{
					throw new Support\Exceptions\TraitNotFoundException("[{$this->model}] is not tokenable");
				}

				$column = $tokenable->getTokenKey();

				if ($length === null)
				{
					$length = $tokenable->getTokenLength();
				}
			}

			return $this->string($column, $length);
		}

		/**
		 * @param int $precision
		 *
		 * @return void
		 */
		public function timestamps($precision = 0) : void
		{
			/** @var string|null $created_at */
			$created_at = ($this->model !== null) ? $this->model::CREATED_AT : 'created_at';
			/** @var string|null $updated_at */
			$updated_at = ($this->model !== null) ? $this->model::UPDATED_AT : 'updated_at';

			if ($created_at !== null)
			{
				$this->timestamp($created_at, $precision)->nullable();
			}

			if ($updated_at !== null)
			{
				$this->timestamp($updated_at, $precision)->nullable();
			}
		}

		/**
		 * @psalm-param class-string<\Illuminate\Database\Eloquent\Model> $relation
		 * @param string|\Illuminate\Database\Eloquent\Model        $relation
		 * @param string|null                                       $column      = null
		 * @psalm-param array<class-string<\Illuminate\Database\Eloquent\Model>> $constraints = []
		 * @param array<string|\Illuminate\Database\Eloquent\Model> $constraints = []
		 *
		 * @return \Illuminate\Database\Schema\ColumnDefinition
		 */
		public function belongsTo(string $relation, string $column = null, array $constraints = []) : Illuminate\Database\Schema\ColumnDefinition
		{
			/** @var \Illuminate\Database\Eloquent\Model $instance */
			$instance = new $relation();

			if ($column === null)
			{
				$column = $instance->getForeignKey();
			}

			/*@$constraints = \array_map(static fn($constraint) => new $constraint(), $constraints);*/
			$constraints = \array_map(static function ($constraint)
				{
					return new $constraint();
				}, $constraints);

			$this->index(\array_merge([$column], $constraints));

			$this->foreign(\array_merge([$column], $constraints))
			     ->references(\array_merge([$instance->getKeyName()], $constraints))
			     ->on($instance->getTable());

			return $this->unsignedInteger($column);
		}

		/**
		 * @param string|\Illuminate\Database\Eloquent\Model $relation
		 * @psalm-param class-string<\Illuminate\Database\Eloquent\Model> $relation
		 *
		 * @return \Illuminate\Support\Fluent
		 */
		public function dropBelongsTo(string $relation) : Illuminate\Support\Fluent
		{
			/** @var \Illuminate\Database\Eloquent\Model $instance */
			$instance = new $relation();

			return $this->dropColumn($instance->getForeignKey());
		}

		/**
		 * @param string|null $column
		 *
		 * @return \Illuminate\Database\Schema\ColumnDefinition
		 * @throws \Codification\Common\Support\Exceptions\ReferenceException
		 */
		public function increments($column = null) : Illuminate\Database\Schema\ColumnDefinition
		{
			if ($column === null)
			{
				$column = $this->ensureInstance()->getKeyName();
			}

			return $this->unsignedInteger($column, true);
		}

		/**
		 * @param string|null $column
		 *
		 * @return \Illuminate\Database\Schema\ColumnDefinition
		 * @throws \Codification\Common\Support\Exceptions\ReferenceException
		 */
		public function bigIncrements($column = null) : Illuminate\Database\Schema\ColumnDefinition
		{
			if ($column === null)
			{
				$column = $this->ensureInstance()->getKeyName();
			}

			return $this->unsignedBigInteger($column, true);
		}
	}
}