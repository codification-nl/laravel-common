<?php

namespace Codification\Common\Database\Eloquent
{
	use Codification\Common\Math;

	/**
	 * @mixin \Illuminate\Database\Eloquent\Concerns\HasAttributes
	 */
	trait HasNumbers
	{
		/**
		 * @psalm-return class-string<\Codification\Common\Math\Number>
		 * @return string
		 */
		public function getHasNumbersType() : string
		{
			return Math\Number::class;
		}

		/**
		 * @return string
		 */
		public function getHasNumbersCast() : string
		{
			return 'number';
		}

		/**
		 * @param string $key
		 *
		 * @return int|null
		 */
		private function getNumberScale(string $key) : ?int
		{
			$cast  = $this->getCasts()[$key];
			$parts = \explode(':', $cast, 2);

			/** @var int|null $scale */
			$scale = $parts[1] ?? null;

			return $scale;
		}

		/**
		 * @template    T
		 *
		 * @param string                                                  $key
		 * @psalm-param T|numeric|null                                    $out
		 * @param-out   T|\Codification\Common\Math\Number                $out
		 * @param mixed|int|float|string|\Codification\Common\Math\Number $out
		 *
		 * @return bool
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function getHasNumbersValue(string $key, &$out) : bool
		{
			if ($out === null || !$this->isNumberAttribute($key))
			{
				return false;
			}

			$scale = $this->getNumberScale($key);
			$out   = $this->asNumber($out, $scale);

			return true;
		}

		/**
		 * @template    T
		 *
		 * @param string                                                  $key
		 * @psalm-param T|\Codification\Common\Math\Number|null           $out
		 * @param-out   T|numeric                                         $out
		 * @param mixed|int|float|string|\Codification\Common\Math\Number $out
		 *
		 * @return bool
		 */
		public function setHasNumbersValue(string $key, &$out) : bool
		{
			if ($out === null || !$this->isNumberAttribute($key))
			{
				return false;
			}

			$out = $this->fromNumber($out);

			return true;
		}

		/**
		 * @psalm-param numeric    $value
		 * @param int|float|string $value
		 * @param int|null         $scale
		 *
		 * @return \Codification\Common\Math\Number
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function asNumber($value, int $scale = null) : Math\Number
		{
			return new Math\Number($value, $scale);
		}

		/**
		 * @param \Codification\Common\Math\Number $value
		 *
		 * @return string
		 */
		public function fromNumber(Math\Number $value) : string
		{
			return $value->getValue();
		}

		/**
		 * @param string $key
		 *
		 * @return bool
		 */
		protected function isNumberCastable(string $key) : bool
		{
			return $this->hasCast($key, $this->getHasNumbersCast());
		}

		/**
		 * @param string $key
		 *
		 * @return bool
		 */
		protected function isNumberAttribute(string $key) : bool
		{
			return $this->isNumberCastable($key);
		}
	}
}