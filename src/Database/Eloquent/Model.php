<?php

namespace Codification\Common\Database\Eloquent
{
	use Codification\Common\Support;
	use Illuminate;

	/**
	 * @psalm-suppress PropertyNotSetInConstructor
	 */
	abstract class Model extends \Illuminate\Database\Eloquent\Model
	{
		/** @var array<class-string, list<callable-string>> */
		protected static /*@array*/
			$traitAccessors = [];

		/** @var array<class-string, list<callable-string>> */
		protected static /*@array*/
			$traitMutators = [];

		/** @var array<class-string, list<callable-string>> */
		protected static /*@array*/
			$traitCasts = [];

		/**
		 * @psalm-return class-string<\Codification\Common\Database\Eloquent\Model>
		 * @return string
		 */
		public function getClass() : string
		{
			return static::class;
		}

		/**
		 * @param array|false $attributes
		 *
		 * @throws \Illuminate\Database\Eloquent\MassAssignmentException
		 */
		public function __construct($attributes = [])
		{
			if ($attributes !== false)
			{
				parent::__construct($attributes);
			}
		}

		/**
		 * @return void
		 */
		protected static function bootTraits() : void
		{
			parent::bootTraits();

			$class = static::class;

			static::$traitAccessors[$class] = [];
			static::$traitMutators[$class]  = [];
			static::$traitCasts[$class]     = [];

			foreach (\class_traits($class) as $trait)
			{
				$name = \class_basename($trait);

				static::addTrait($class, "get{$name}Value", static::$traitAccessors);
				static::addTrait($class, "set{$name}Value", static::$traitMutators);
				static::addTrait($class, "get{$name}Cast", static::$traitCasts);
			}
		}

		/**
		 * @psalm-param  class-string          $class
		 * @param string                       $class
		 * @param string                       $method
		 * @param-out    non-empty- array<class-string, list<callable-string>> $target
		 * @param array<string, array<string>> $target
		 *
		 * @return void
		 */
		private static function addTrait(string $class, string $method, array &$target) : void
		{
			if (\in_array($method, $target[$class], true) || !\method_exists($class, $method))
			{
				return;
			}

			$target[$class][] = $method;
		}

		/**
		 * @return string
		 */
		public function getQualifiedTable() : string
		{
			return "{$this->getConnectionName()}.{$this->getTable()}";
		}

		/**
		 * @param string $key
		 *
		 * @return string
		 */
		protected function getCastType($key) : string
		{
			$type = parent::getCastType($key);

			foreach (static::$traitCasts[$this->getClass()] as $method)
			{
				/** @var string $custom */
				$custom = $this->{$method}();

				if (\strncmp($type, $custom, \strlen($custom)) === 0)
				{
					return $custom;
				}
			}

			return $type;
		}

		/**
		 * @template     TValue
		 *
		 * @param string $key
		 *
		 * @psalm-return TValue
		 * @return mixed
		 */
		public function getAttributeValue($key)
		{
			/** @psalm-var TValue $value */
			$value = parent::getAttributeValue($key);

			foreach (static::$traitAccessors[$this->getClass()] as $method)
			{
				if ($this->{$method}($key, $value))
				{
					return $value;
				}
			}

			return $value;
		}

		/**
		 * @template     TValue
		 *
		 * @param string $key
		 * @psalm-param  TValue $value
		 * @param mixed  $value
		 *
		 * @return mixed
		 * @throws \Illuminate\Database\Eloquent\JsonEncodingException
		 */
		public function setAttribute($key, $value)
		{
			foreach (static::$traitMutators[$this->getClass()] as $method)
			{
				if ($this->{$method}($key, $value))
				{
					break;
				}
			}

			return parent::setAttribute($key, $value);
		}

		/**
		 * @return self
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function dummy() : self
		{
			try
			{
				return new static(false);
			}
			catch (Illuminate\Database\Eloquent\MassAssignmentException $e)
			{
				throw new Support\Exceptions\ShouldNotHappenException('Failed to instantiate dummy', $e);
			}
		}
	}
}