<?php

namespace Codification\Common\Database\Eloquent
{
	use Codification\Common\Enum;

	/**
	 * @mixin \Illuminate\Database\Eloquent\Concerns\HasAttributes
	 */
	trait HasEnums
	{
		/**
		 * @return void
		 */
		public static function bootHasEnums() : void
		{
			/** @psalm-suppress MissingThrowsDocblock */
			static::addGlobalScope(new EnumScope());
		}

		/**
		 * @template    TValue of array-key
		 * @template    T
		 *
		 * @param string                                          $key
		 * @psalm-param T|TValue|null                                  $out
		 * @param-out   T|\Codification\Common\Enum\Enum<TValue>       $out
		 * @param mixed|int|string|\Codification\Common\Enum\Enum $out
		 *
		 * @return bool
		 *
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function getHasEnumsValue(string $key, &$out) : bool
		{
			if ($out === null || !$this->isEnumAttribute($key))
			{
				return false;
			}

			/** @psalm-var TValue $value */
			$value = $out;
			$class = $this->getEnum($key);

			$out = $this->asEnum($value, $class);

			return true;
		}

		/**
		 * @template    TValue of array-key
		 * @template    T
		 *
		 * @param string                                          $key
		 * @psalm-param T|\Codification\Common\Enum\Enum<TValue>|null  $out
		 * @param-out   T|TValue                                       $out
		 * @param mixed|int|string|\Codification\Common\Enum\Enum $out
		 *
		 * @return bool
		 *
		 * @throws \Codification\Common\Enum\Exceptions\TypeException
		 */
		public function setHasEnumsValue(string $key, &$out) : bool
		{
			if ($out === null || !$this->isEnumAttribute($key))
			{
				return false;
			}

			/** @psalm-var \Codification\Common\Enum\Enum<TValue> $enum */
			$enum  = $out;
			$class = $this->getEnum($key);

			$class::assertType($enum);

			$out = $this->fromEnum($enum);

			return true;
		}

		/**
		 * @psalm-return array<string, class-string<\Codification\Common\Enum\Enum<array-key>>>
		 * @return array<string, string|\Codification\Common\Enum\Enum>
		 */
		public function getEnums() : array
		{
			return $this->enums;
		}

		/**
		 * @template     TValue of array-key
		 *
		 * @param string $key
		 *
		 * @psalm-return class-string<\Codification\Common\Enum\Enum<TValue>>
		 * @return string|\Codification\Common\Enum\Enum
		 */
		private function getEnum(string $key) : string
		{
			/**
			 * @psalm-var class-string<\Codification\Common\Enum\Enum<TValue>> $enum
			 * @var string|\Codification\Common\Enum\Enum $enum
			 */
			$enum = $this->enums[$key];

			return $enum;
		}

		/**
		 * @template    TValue of array-key
		 *
		 * @psalm-param TValue                               $value
		 * @param int|string                            $value
		 * @psalm-param class-string<\Codification\Common\Enum\Enum<TValue>> $class
		 * @param string|\Codification\Common\Enum\Enum $class
		 *
		 * @psalm-return \Codification\Common\Enum\Enum<TValue>
		 * @return \Codification\Common\Enum\Enum
		 *
		 * @throws \Codification\Common\Enum\Exceptions\ValueException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function asEnum($value, string $class) : Enum\Enum
		{
			/**
			 * @psalm-var \Codification\Common\Enum\Enum<TValue> $enum
			 * @var \Codification\Common\Enum\Enum $enum
			 */
			$enum = $class::make($value);

			return $enum;
		}

		/**
		 * @template     TValue of array-key
		 *
		 * @psalm-param \Codification\Common\Enum\Enum<TValue> $enum
		 * @param \Codification\Common\Enum\Enum $enum
		 *
		 * @psalm-return TValue
		 * @return int|string
		 */
		public function fromEnum(Enum\Enum $enum)
		{
			/**
			 * @psalm-var TValue    $value
			 * @var int|string $value
			 */
			$value = $enum->getValue();

			return $value;
		}

		/**
		 * @param string $key
		 *
		 * @return bool
		 */
		protected function isEnumCastable(string $key) : bool
		{
			return \array_key_exists($key, $this->enums);
		}

		/**
		 * @param string $key
		 *
		 * @return bool
		 */
		protected function isEnumAttribute(string $key) : bool
		{
			return $this->isEnumCastable($key);
		}
	}
}