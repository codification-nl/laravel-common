<?php

namespace Codification\Common\Database\Eloquent\Contracts
{
	use Codification\Common\Enum;

	/**
	 * @psalm-property array<string, class-string<\Codification\Common\Enum\Enum<array-key>>> $enums
	 * @property array<string, string|\Codification\Common\Enum\Enum> $enums
	 */
	interface HasEnums
	{
		/**
		 * @psalm-return array<string, class-string<\Codification\Common\Enum\Enum<array-key>>>
		 * @return array<string, string|\Codification\Common\Enum\Enum>
		 */
		public function getEnums() : array;

		/**
		 * @template    TValue of array-key
		 *
		 * @psalm-param TValue                               $value
		 * @param int|string                            $value
		 * @psalm-param class-string<\Codification\Common\Enum\Enum<TValue>> $class
		 * @param string|\Codification\Common\Enum\Enum $class
		 *
		 * @psalm-return \Codification\Common\Enum\Enum<TValue>
		 * @return \Codification\Common\Enum\Enum
		 */
		public function asEnum($value, string $class) : Enum\Enum;

		/**
		 * @template     TValue of array-key
		 *
		 * @psalm-param \Codification\Common\Enum\Enum<TValue> $enum
		 * @param \Codification\Common\Enum\Enum $enum
		 *
		 * @psalm-return TValue
		 * @return int|string
		 */
		public function fromEnum(Enum\Enum $enum);
	}
}