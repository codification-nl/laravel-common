<?php

namespace Codification\Common\Database\Eloquent\Contracts
{
	/**
	 * @template T
	 *
	 * @property string $tokenKey
	 */
	interface Tokenable
	{
		/**
		 * @return int
		 */
		public function getTokenLength() : int;

		/**
		 * @return string
		 */
		public function getTokenKey() : string;

		/**
		 * @param int $length
		 *
		 * @psalm-return T
		 * @return mixed
		 */
		public function generateToken(int $length);

		/**
		 * @psalm-return T
		 * @return mixed
		 */
		public function getToken();
	}
}