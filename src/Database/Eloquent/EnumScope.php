<?php

namespace Codification\Common\Database\Eloquent
{
	use Illuminate;
	use Codification\Common\Enum;

	use Illuminate\Database\Eloquent\
	{
		Model as EloquentModel,
		Builder as EloquentBuilder
	};

	class EnumScope implements Illuminate\Database\Eloquent\Scope
	{
		/**
		 * @param \Illuminate\Database\Eloquent\Builder $builder
		 * @param \Illuminate\Database\Eloquent\Model   $model
		 *
		 * @return void
		 */
		public function apply(EloquentBuilder $builder, EloquentModel $model) : void
		{
			/** @var \Codification\Common\Database\Eloquent\Contracts\HasEnums $model */
			foreach ($model->getEnums() as $column => $type)
			{
				if (!\in_array(Enum\EnumFlags::class, \class_traits($type), true))
				{
					continue;
				}

				$builder = self::select($builder, $column);
			}
		}

		/**
		 * @template T of array-key
		 *
		 * @param \Illuminate\Database\Eloquent\Builder $builder
		 *
		 * @return void
		 */
		public function extend(EloquentBuilder $builder) : void
		{
			/** @var \Codification\Common\Database\Eloquent\Contracts\HasEnums $model */
			$model = $builder->getModel();

			foreach ($model->getEnums() as $column => $type)
			{
				if (!\in_array(Enum\EnumFlags::class, \class_traits($type), true))
				{
					continue;
				}

				$name = 'where' . Illuminate\Support\Str::studly($column) . 'Has';

				$builder->macro(
					$name,
					/**
					 * @param \Illuminate\Database\Eloquent\Builder     $builder
					 * @psalm-param \Codification\Common\Enum\Enum<T>|T $value
					 * @param \Codification\Common\Enum\Enum|int|string $value
					 * @param string                                    $boolean = 'and'
					 *
					 * @return \Illuminate\Database\Eloquent\Builder
					 */
					static function (EloquentBuilder $builder, $value, string $boolean = 'and') use ($column)
						{
							return self::where($builder, $column, $value, $boolean);
						}
				/*@static fn(EloquentBuilder $builder, $value, string $boolean = 'and') => self::where($builder, $column, $value, $boolean)*/
				);
			}
		}

		/**
		 * @param \Illuminate\Database\Eloquent\Builder $builder
		 * @param string                                $column
		 *
		 * @return \Illuminate\Database\Eloquent\Builder
		 *
		 * @noinspection BadExceptionsProcessingInspection
		 */
		public static function select(EloquentBuilder $builder, string $column) : EloquentBuilder
		{
			try
			{
				$query = $builder->getQuery();

				if (\count((array)$query->columns) === 0)
				{
					$builder->addSelect(['*']);
				}

				$qualified = $builder->qualifyColumn($column);
				$wrapped   = $query->grammar->wrap($qualified);
				$alias     = $query->grammar->wrap($column);

				$builder->selectRaw("({$wrapped} | 0) as {$alias}");
			}
			catch (\RuntimeException $e)
			{
			}
			catch (\InvalidArgumentException $e)
			{
			}

			return $builder;
		}

		/**
		 * @template     T of array-key
		 *
		 * @param \Illuminate\Database\Eloquent\Builder     $builder
		 * @param string                                    $column
		 * @psalm-param \Codification\Common\Enum\Enum<T>|T $value
		 * @param \Codification\Common\Enum\Enum|int|string $value
		 * @param string                                    $boolean = 'and'
		 *
		 * @return \Illuminate\Database\Eloquent\Builder
		 *
		 * @noinspection BadExceptionsProcessingInspection
		 */
		public static function where(EloquentBuilder $builder, string $column, $value, string $boolean = 'and') : EloquentBuilder
		{
			try
			{
				$query = $builder->getQuery();

				$qualified = $builder->qualifyColumn($column);
				$wrapped   = $query->grammar->wrap($qualified);

				$builder->whereRaw("({$wrapped} & ?) != 0", [$value], $boolean);
			}
			catch (\RuntimeException $e)
			{
			}
			catch (\InvalidArgumentException $e)
			{
			}

			return $builder;
		}
	}
}