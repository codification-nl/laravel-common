<?php

namespace Codification\Common\Database\Eloquent
{
	use Illuminate;
	use Codification\Common\Support;

	/**
	 * @template T
	 * @mixin \Illuminate\Database\Eloquent\Concerns\HasAttributes
	 */
	trait HasToken
	{
		/**
		 * @return void
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function initializeHasToken() : void
		{
			$this->attributes[$this->getTokenKey()] = $this->generateToken($this->getTokenLength());
		}

		/**
		 * @return string
		 */
		public function getRouteKeyName() : string
		{
			return $this->getTokenKey();
		}

		/**
		 * @return int
		 */
		public function getTokenLength() : int
		{
			return \defined('static::TOKEN_LENGTH') ? (int)static::TOKEN_LENGTH : 60;
		}

		/**
		 * @return string
		 */
		public function getTokenKey() : string
		{
			return \property_exists($this, 'tokenKey') ? (string)($this->tokenKey) : 'token';
		}

		/**
		 * @psalm-return   T
		 * @return mixed
		 *
		 * @psalm-suppress MixedInferredReturnType
		 */
		public function getToken()
		{
			return $this->{$this->getTokenKey()};
		}

		/**
		 * @param int $length
		 *
		 * @psalm-return T
		 * @return mixed
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function generateToken(int $length)
		{
			try
			{
				return Illuminate\Support\Str::random($length);
			}
			catch (\Exception $e)
			{
				throw new Support\Exceptions\ShouldNotHappenException('Failed to generate token', $e);
			}
		}
	}
}