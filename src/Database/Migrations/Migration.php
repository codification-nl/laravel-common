<?php

namespace Codification\Common\Database\Migrations
{
	use Illuminate;
	use Codification\Common\Database;
	use Codification\Common\Support;

	/**
	 * @template T of \Illuminate\Database\Eloquent\Model
	 */
	abstract class Migration extends \Illuminate\Database\Migrations\Migration
	{
		/** @var string|null */
		protected /*@?string*/
			$table;

		/**
		 * @var string|\Illuminate\Database\Eloquent\Model|null
		 * @psalm-var class-string<T>|null
		 */
		protected /*@?string*/
			$model;

		/**
		 * @var \Illuminate\Database\Eloquent\Model|null
		 * @psalm-var T|null
		 */
		protected /*@?Illuminate\Database\Eloquent\Model*/
			$instance;

		/** @var \Codification\Common\Database\Schema\Builder */
		protected /*@Database\Schema\Builder*/
			$schema;

		/**
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \Codification\Common\Support\Exceptions\ReferenceException
		 */
		public function __construct()
		{
			$this->table = null;

			if ($this->model !== null)
			{
				$this->instance   = new $this->model();
				$this->table      = $this->instance->getTable();
				$this->connection = $this->instance->getConnectionName();
			}

			if ($this->table === null)
			{
				throw new Support\Exceptions\ReferenceException('$this->table');
			}

			if ($this->connection === null)
			{
				throw new Support\Exceptions\ReferenceException('$this->connection');
			}

			try
			{
				/** @var \Illuminate\Database\DatabaseManager $db */
				$db = Support\ContainerUtils::resolve('db');
			}
			catch (Support\Exceptions\ResolutionException $e)
			{
				throw new Support\Exceptions\ShouldNotHappenException('Failed to resolve [db]', $e->getPrevious());
			}

			$connection = $db->connection($this->connection);

			$this->schema = new Database\Schema\Builder($connection, $this->table, $this->model, $this->instance);
		}

		/**
		 * @return void
		 */
		abstract public function up() : void;

		/**
		 * @return void
		 */
		abstract public function down() : void;
	}
}