<?php

namespace Codification\Common\Contracts\Log
{
	use Illuminate;

	interface Loggable
	{
		/**
		 * @return \Illuminate\Database\Eloquent\Relations\MorphMany
		 */
		public function logs() : Illuminate\Database\Eloquent\Relations\MorphMany;
	}
}