<?php

namespace Codification\Common\Contracts\Support
{
	interface Stringable extends \JsonSerializable
	{
		/**
		 * @return string
		 */
		public function toString() : string;

		/**
		 * @return string
		 */
		public function __toString() : string;
	}
}