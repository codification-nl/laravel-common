<?php

namespace Codification\Common\Contracts\Support
{
	/**
	 * @template T
	 */
	interface Cloneable
	{
		/**
		 * @psalm-return T
		 */
		public function copy();

		/**
		 * @psalm-return T
		 */
		public function clone();

		/**
		 * @psalm-return T
		 */
		public function __clone();
	}
}