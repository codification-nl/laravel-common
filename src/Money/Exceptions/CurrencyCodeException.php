<?php

namespace Codification\Common\Money\Exceptions
{
	final class CurrencyCodeException extends \DomainException
	{
		/**
		 * @param string|null $code = null
		 */
		public function __construct(string $code = null)
		{
			if ($code === null)
			{
				$code = 'null';
			}

			parent::__construct("[{$code}] is invalid");
		}
	}
}