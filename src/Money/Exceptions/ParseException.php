<?php

namespace Codification\Common\Money\Exceptions
{
	final class ParseException extends \RuntimeException
	{
		/**
		 * @param mixed           $value
		 * @param \Throwable|null $previous = null
		 */
		public function __construct($value, \Throwable $previous = null)
		{
			parent::__construct("Unable to parse {$value}", 0, $previous);
		}
	}
}