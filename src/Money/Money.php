<?php

namespace Codification\Common\Money
{
	use Codification\Common\Contracts;
	use Codification\Common\Support;
	use Money as BaseMoney;

	/**
	 * @method bool equals(\Codification\Common\Money\Money $other)
	 * @method bool greaterThan(\Codification\Common\Money\Money $other)
	 * @method bool greaterThanOrEqual(\Codification\Common\Money\Money $other)
	 * @method bool lessThan(\Codification\Common\Money\Money $other)
	 * @method bool lessThanOrEqual(\Codification\Common\Money\Money $other)
	 * @method bool isSameCurrency(\Codification\Common\Money\Money $other)
	 * @method \Codification\Common\Money\Money add(\Codification\Common\Money\Money $other)
	 * @method \Codification\Common\Money\Money subtract(\Codification\Common\Money\Money $other)
	 * @method \Codification\Common\Money\Money multiply(float|int|string $other, int $rounding_mode = \PHP_ROUND_HALF_UP)
	 * @method \Codification\Common\Money\Money divide(float|int|string $other, int $rounding_mode = \PHP_ROUND_HALF_UP)
	 * @method \Codification\Common\Money\Money mod(\Codification\Common\Money\Money $other)
	 * @method \Codification\Common\Money\Money[] allocate(int[]|float[] $ratios)
	 * @method \Codification\Common\Money\Money[] allocateTo(int $n)
	 * @method \Codification\Common\Money\Money ratioOf(\Codification\Common\Money\Money $other)
	 * @method \Codification\Common\Money\Money absolute()
	 * @method \Codification\Common\Money\Money negative()
	 * @method bool isZero()
	 * @method bool isPositive()
	 * @method bool isNegative()
	 * @method static \Codification\Common\Money\Money min(\Codification\Common\Money\Money ...$values)
	 * @method static \Codification\Common\Money\Money max(\Codification\Common\Money\Money ...$values)
	 * @method static \Codification\Common\Money\Money avg(\Codification\Common\Money\Money ...$values)
	 * @method static \Codification\Common\Money\Money sum(\Codification\Common\Money\Money ...$values)
	 *
	 * @template-implements \Codification\Common\Contracts\Support\Cloneable<\Codification\Common\Money\Money>
	 */
	final class Money implements Contracts\Support\Stringable, Contracts\Support\Cloneable, Support\Contracts\Bindable
	{
		/** @var null|\Money\Money */
		private /*@?BaseMoney\Money*/
			$instance = null;

		/**
		 * @param string|\Money\Currency $currency
		 * @param string|null            $locale = null
		 *
		 * @return \Codification\Common\Money\Money
		 *
		 * @throws \Codification\Common\Money\Exceptions\CurrencyCodeException
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \Codification\Common\Money\Exceptions\ParseException
		 */
		public static function zero($currency, string $locale = null) : self
		{
			$zero = self::make(0, $currency, $locale);

			if ($zero === null)
			{
				throw new Support\Exceptions\ShouldNotHappenException('Failed to instantiate [Money]');
			}

			return $zero;
		}

		/**
		 * @psalm-param numeric|null     $value
		 * @param string|float|int|null  $value
		 * @param string|\Money\Currency $currency
		 * @param string|null            $locale = null
		 *
		 * @return \Codification\Common\Money\Money|null
		 *
		 * @throws \Codification\Common\Money\Exceptions\CurrencyCodeException
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \Codification\Common\Money\Exceptions\ParseException
		 */
		public static function make($value, $currency, string $locale = null) : ?self
		{
			$instance = MoneyUtils::getInstance()->parse($value, $currency, $locale);

			if ($instance === null)
			{
				return null;
			}

			$money = new self();

			$money->instance = $instance;

			return $money;
		}

		/**
		 * @return string
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \Codification\Common\Money\Exceptions\CurrencyCodeException
		 */
		public function format() : string
		{
			return MoneyUtils::getInstance()->format($this->ensureInstance());
		}

		/**
		 * @param string|null $locale = null
		 *
		 * @return string
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \Codification\Common\Money\Exceptions\CurrencyCodeException
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 */
		public function humanize(string $locale = null) : string
		{
			return MoneyUtils::getInstance()->humanize($this->ensureInstance(), $locale);
		}

		/**
		 * @return int
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 * @throws \Codification\Common\Money\Exceptions\CurrencyCodeException
		 */
		public function getCurrencyCode() : int
		{
			return MoneyUtils::getInstance()->getCurrencyCode($this->ensureInstance());
		}

		/**
		 * @return string
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function getAmount() : string
		{
			return $this->ensureInstance()->getAmount();
		}

		/**
		 * @return \Money\Currency
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function getCurrency() : BaseMoney\Currency
		{
			return $this->ensureInstance()->getCurrency();
		}

		/**
		 * @param string $name
		 * @param array  $parameters
		 *
		 * @return mixed
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function __call(string $name, array $parameters)
		{
			return self::call([$this->ensureInstance(), $name], $parameters);
		}

		/**
		 * @param string $name
		 * @param array  $parameters
		 *
		 * @return mixed
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public static function __callStatic(string $name, array $parameters)
		{
			return self::call([BaseMoney\Money::class, $name], $parameters);
		}

		/**
		 * @psalm-param  array{0: \Money\Money|class-string<\Money\Money>, 1: string} $function
		 * @param array $function
		 * @param array $parameters
		 *
		 * @return mixed
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		private static function call($function, array $parameters)
		{
			/**
			 * @psalm-var list $params
			 * @var mixed[] $params
			 */
			$params = self::unwrap($parameters);

			/**
			 * @var mixed $result
			 */
			$result = \call_user_func_array($function, $params);

			return self::wrap($result);
		}

		/**
		 * @param mixed|mixed[] $value
		 *
		 * @return mixed|mixed[]
		 */
		private static function wrap($value)
		{
			if (\is_array($value))
			{
				/** @psalm-suppress MissingClosureReturnType */
				return \array_map(static function ($item)
					{
						return self::wrap($item);
					}, $value);
				/*@return \array_map(static fn($item) => self::wrap($item), $value);*/
			}

			if ($value instanceof BaseMoney\Money)
			{
				$result = new self();

				$result->instance = clone $value;

				return $result;
			}

			return $value;
		}

		/**
		 * @param mixed|mixed[] $value
		 *
		 * @return mixed|mixed[]
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		private static function unwrap($value)
		{
			if (\is_array($value))
			{
				/** @psalm-suppress MissingClosureReturnType */
				return \array_map(static function ($item)
					{
						return self::unwrap($item);
					}, $value);
				/*@return \array_map(static fn($item) => self::unwrap($item), $value);*/
			}

			if ($value instanceof self)
			{
				return $value->ensureInstance();
			}

			return $value;
		}

		/**
		 * @return self
		 */
		public function copy() : self
		{
			return clone $this;
		}

		/**
		 * @return self
		 */
		public function clone() : self
		{
			return clone $this;
		}

		/**
		 * @return string
		 *
		 * @throws \Codification\Common\Money\Exceptions\CurrencyCodeException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function jsonSerialize() : string
		{
			return $this->format();
		}

		/**
		 * @return string
		 *
		 * @throws \Codification\Common\Money\Exceptions\CurrencyCodeException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function __toString() : string
		{
			return $this->toString();
		}

		/**
		 * @return string
		 *
		 * @throws \Codification\Common\Money\Exceptions\CurrencyCodeException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function toString() : string
		{
			return $this->format();
		}

		/**
		 * @return \Codification\Common\Money\Money
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function __clone()
		{
			$amount   = $this->getAmount();
			$currency = $this->getCurrency();

			try
			{
				/** @var \Codification\Common\Money\Money $clone */
				$clone = self::wrap(new BaseMoney\Money($amount, $currency));
			}
			catch (\InvalidArgumentException $e)
			{
				throw new Support\Exceptions\ShouldNotHappenException('Failed to instantiate [Money]', $e);
			}

			return $clone;
		}

		/**
		 * @return \Money\Money
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		private function ensureInstance() : BaseMoney\Money
		{
			if ($this->instance === null)
			{
				throw new Support\Exceptions\ShouldNotHappenException('$this->instance === null');
			}

			return $this->instance;
		}
	}
}