<?php

namespace Codification\Common\Money
{
	use Codification\Common\Support;
	use Money as BaseMoney;

	final class MoneyUtils
	{
		/** @var \Codification\Common\Money\MoneyUtils|null */
		private static /*@?self*/
			$instance = null;

		/** @var \Money\Currencies\ISOCurrencies */
		private /*@BaseMoney\Currencies\ISOCurrencies*/
			$isoCurrencies;

		/** @var \Money\Formatter\DecimalMoneyFormatter */
		private /*@BaseMoney\Formatter\DecimalMoneyFormatter*/
			$decimalFormatter;

		/** @var \Money\Parser\DecimalMoneyParser */
		private /*@BaseMoney\Parser\DecimalMoneyParser*/
			$decimalParser;

		/** @var array<string, \Money\Parser\AggregateMoneyParser> */
		private /*@array*/
			$parsers = [];

		/** @var array<string, \Money\Formatter\IntlMoneyFormatter> */
		private /*@array*/
			$formatters = [];

		/** @var array<string, \Money\Currency> */
		private /*@array*/
			$currencies = [];

		/**
		 * MoneyUtils constructor.
		 */
		private function __construct()
		{
			$this->isoCurrencies    = new BaseMoney\Currencies\ISOCurrencies();
			$this->decimalParser    = new BaseMoney\Parser\DecimalMoneyParser($this->isoCurrencies);
			$this->decimalFormatter = new BaseMoney\Formatter\DecimalMoneyFormatter($this->isoCurrencies);
		}

		/**
		 * @return \Codification\Common\Money\MoneyUtils
		 */
		public static function getInstance() : self
		{
			if (self::$instance === null)
			{
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * @param \Money\Money $instance
		 *
		 * @return int
		 *
		 * @throws \Codification\Common\Money\Exceptions\CurrencyCodeException
		 *
		 * @noinspection BadExceptionsProcessingInspection
		 */
		public function getCurrencyCode(BaseMoney\Money $instance) : int
		{
			try
			{
				return $this->isoCurrencies->numericCodeFor($instance->getCurrency());
			}
			catch (BaseMoney\Exception\UnknownCurrencyException $e)
			{
				throw new Exceptions\CurrencyCodeException();
			}
		}

		/**
		 * @param \Money\Money $instance
		 *
		 * @return string
		 *
		 * @throws \Codification\Common\Money\Exceptions\CurrencyCodeException
		 *
		 * @noinspection BadExceptionsProcessingInspection
		 */
		public function format(BaseMoney\Money $instance) : string
		{
			try
			{
				return $this->decimalFormatter->format($instance);
			}
			catch (BaseMoney\Exception\UnknownCurrencyException $e)
			{
				throw new Exceptions\CurrencyCodeException();
			}
		}

		/**
		 * @param \Money\Money $instance
		 * @param string|null  $locale = null
		 *
		 * @return string
		 *
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 * @throws \Codification\Common\Money\Exceptions\CurrencyCodeException
		 *
		 * @noinspection BadExceptionsProcessingInspection
		 */
		public function humanize(BaseMoney\Money $instance, string $locale = null) : string
		{
			try
			{
				return $this->getFormatter($locale)->format($instance);
			}
			catch (BaseMoney\Exception\UnknownCurrencyException $e)
			{
				throw new Exceptions\CurrencyCodeException();
			}
		}

		/**
		 * @param string $code
		 *
		 * @return \Money\Currency
		 *
		 * @throws \Codification\Common\Money\Exceptions\CurrencyCodeException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		private function getCurrency(string $code) : BaseMoney\Currency
		{
			$code = \sanitize($code);

			if ($code === null)
			{
				throw new Exceptions\CurrencyCodeException();
			}

			$code = \strtoupper($code);

			if (!array_key_exists($code, $this->currencies))
			{
				try
				{
					$currency = new BaseMoney\Currency($code);
				}
				catch (\InvalidArgumentException $e)
				{
					throw new Support\Exceptions\ShouldNotHappenException('Failed to instantiate [Currency]', $e);
				}

				if (!$this->isoCurrencies->contains($currency))
				{
					throw new Exceptions\CurrencyCodeException($currency->getCode());
				}

				$this->currencies[$code] = $currency;
			}

			return $this->currencies[$code];
		}

		/**
		 * @param string|null $locale = null
		 *
		 * @return \Money\Parser\AggregateMoneyParser
		 *
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		private function getParser(string $locale = null) : BaseMoney\Parser\AggregateMoneyParser
		{
			$locale = Support\ContainerUtils::resolveLocale($locale);

			if (!array_key_exists($locale, $this->parsers))
			{
				$formatter   = new \NumberFormatter($locale, \NumberFormatter::DECIMAL);
				$intl_parser = new BaseMoney\Parser\IntlLocalizedDecimalParser($formatter, $this->isoCurrencies);

				try
				{
					$this->parsers[$locale] = new BaseMoney\Parser\AggregateMoneyParser([
						$this->decimalParser,
						$intl_parser,
					]);
				}
				catch (\InvalidArgumentException $e)
				{
					throw new Support\Exceptions\ShouldNotHappenException('Failed to instantiate parser', $e);
				}
			}

			return $this->parsers[$locale];
		}

		/**
		 * @param string|null $locale = null
		 *
		 * @return \Money\Formatter\IntlMoneyFormatter
		 *
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 */
		private function getFormatter(string $locale = null) : BaseMoney\Formatter\IntlMoneyFormatter
		{
			$locale = Support\ContainerUtils::resolveLocale($locale);

			if (!array_key_exists($locale, $this->formatters))
			{
				$formatter      = new \NumberFormatter($locale, \NumberFormatter::CURRENCY);
				$intl_formatter = new BaseMoney\Formatter\IntlMoneyFormatter($formatter, $this->isoCurrencies);

				$this->formatters[$locale] = $intl_formatter;
			}

			return $this->formatters[$locale];
		}

		/**
		 * @psalm-param numeric|null     $value
		 * @param string|float|int|null  $value
		 * @param string|\Money\Currency $currency
		 * @param string|null            $locale = null
		 *
		 * @return \Money\Money
		 *
		 * @throws \Codification\Common\Money\Exceptions\CurrencyCodeException
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 * @throws \Codification\Common\Money\Exceptions\ParseException
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function parse($value, $currency, string $locale = null) : ?BaseMoney\Money
		{
			$value = \sanitize($value);

			if ($value === null)
			{
				return null;
			}

			if (\is_string($currency))
			{
				$currency = $this->getCurrency($currency);
			}

			try
			{
				return $this->getParser($locale)->parse($value, $currency);
			}
			catch (BaseMoney\Exception\ParserException $e)
			{
				throw new Exceptions\ParseException($value, $e->getPrevious());
			}
		}
	}
}