<?php

namespace Codification\Common\Support
{
	use Illuminate;

	/**
	 * @template            TKey of array-key
	 * @template            T
	 *
	 * @template-implements \ArrayAccess<TKey, T>
	 * @template-implements \IteratorAggregate<TKey, T>
	 */
	abstract class Dictionary implements \ArrayAccess, Illuminate\Contracts\Support\Arrayable, Illuminate\Contracts\Support\Jsonable, \JsonSerializable, \Countable, \IteratorAggregate
	{
		/**
		 * @psalm-var array<TKey, T>
		 * @var array
		 */
		protected /*@array*/
			$items = [];

		/**
		 * @psalm-param array<TKey, T> $items
		 * @param array $items
		 */
		protected function __construct(array $items)
		{
			$this->items = $items;
		}

		/**
		 * @psalm-return array<TKey, T>
		 * @return array
		 */
		public function all() : array
		{
			return $this->items;
		}

		/**
		 * @psalm-param TKey $key
		 * @param mixed $key
		 *
		 * @return bool
		 */
		public function has($key) : bool
		{
			return array_key_exists($key, $this->items);
		}

		/**
		 * @psalm-param TKey $key
		 * @param mixed $key
		 *
		 * @return void
		 */
		public function remove($key) : void
		{
			unset($this->items[$key]);
		}

		/**
		 * @psalm-param array<TKey, T> $items
		 * @param array $items
		 *
		 * @return void
		 */
		public function add(array $items) : void
		{
			$this->items = \array_replace($this->items, $items);
		}

		/**
		 * @template     TValue
		 *
		 * @psalm-param  TKey $key
		 * @param mixed $key
		 * @psalm-param  TValue|null $default = null
		 * @param mixed $default = null
		 *
		 * @psalm-return T|TValue|null
		 * @return mixed|null
		 */
		public function get($key, $default = null)
		{
			return $this->items[$key] ?? $default;
		}

		/**
		 * @psalm-param TKey $key
		 * @param mixed $key
		 * @psalm-param T $value
		 * @param mixed $value
		 *
		 * @return void
		 */
		public function set($key, $value) : void
		{
			$this->items[$key] = $value;
		}

		/**
		 * @psalm-return list<TKey>
		 * @return mixed[]
		 */
		public function keys() : array
		{
			return array_keys($this->items);
		}

		/**
		 * @psalm-return list<T>
		 * @return mixed[]
		 */
		public function values() : array
		{
			return \array_values($this->items);
		}

		/**
		 * @psalm-param TKey $key
		 * @param mixed $key
		 *
		 * @return bool
		 */
		public function offsetExists($key) : bool
		{
			return $this->has($key);
		}

		/**
		 * @psalm-param  TKey $key
		 * @param mixed $key
		 *
		 * @psalm-return T|null
		 * @return mixed|null
		 */
		public function offsetGet($key)
		{
			return $this->get($key);
		}

		/**
		 * @psalm-param TKey $key
		 * @param mixed $key
		 * @psalm-param T $value
		 * @param mixed $value
		 *
		 * @return void
		 */
		public function offsetSet($key, $value) : void
		{
			$this->set($key, $value);
		}

		/**
		 * @psalm-param TKey $key
		 * @param mixed $key
		 *
		 * @return void
		 */
		public function offsetUnset($key) : void
		{
			$this->remove($key);
		}

		/**
		 * @psalm-return array<TKey, T>
		 * @return array
		 */
		public function jsonSerialize() : array
		{
			return $this->toArray();
		}

		/**
		 * @psalm-return array<TKey, T>
		 * @return array
		 */
		public function toArray() : array
		{
			return $this->items;
		}

		/**
		 * @return int
		 */
		public function count() : int
		{
			return \count($this->items);
		}

		/**
		 * @psalm-return \ArrayIterator<TKey, T>
		 * @return \ArrayIterator
		 */
		public function getIterator() : \ArrayIterator
		{
			return new \ArrayIterator($this->items);
		}

		/**
		 * @param int $options = 0
		 *
		 * @return string
		 *
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function toJson($options = 0) : string
		{
			$json = \json_encode($this->jsonSerialize(), $options | \JSON_THROW_ON_ERROR);

			if ($json === false)
			{
				throw new Exceptions\ShouldNotHappenException('Failed to get JSON representation');
			}

			return $json;
		}
	}
}