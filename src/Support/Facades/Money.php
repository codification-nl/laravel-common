<?php

namespace Codification\Common\Support\Facades
{
	/**
	 * @method static \Codification\Common\Money\Money|null make(string|float|int|null $value, string|\Money\Currency $currency, string|null $locale = null)
	 * @method static \Codification\Common\Money\Money zero(string|\Money\Currency $currency, string|null $locale = null)
	 *
	 * @see \Codification\Common\Money\Money
	 */
	final class Money extends \Illuminate\Support\Facades\Facade
	{
		/**
		 * @return string
		 */
		protected static function getFacadeAccessor() : string
		{
			return 'money';
		}
	}
}