<?php

namespace Codification\Common\Support\Facades
{
	/**
	 * @method static bool isValid(string|null $country_code)
	 * @method static void ensureValid(string|null $country_code)
	 *
	 * @see \Codification\Common\Country\Country
	 */
	final class Country extends \Illuminate\Support\Facades\Facade
	{
		/**
		 * @return string
		 */
		protected static function getFacadeAccessor() : string
		{
			return 'country';
		}
	}
}