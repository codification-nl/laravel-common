<?php

namespace Codification\Common\Support\Facades
{
	/**
	 * @method static \Codification\Common\Url\UrlParser parse(string|null $url, \Codification\Common\Url\UrlParseFlags|int $flags = \Codification\Common\Url\UrlParseFlags::ALL)
	 * @method static string|null sanitize(string|null $url, \Codification\Common\Url\UrlParseFlags|int $flags = \Codification\Common\Url\UrlParseFlags::ALL)
	 *
	 * @see \Codification\Common\Url\UrlParser
	 */
	final class UrlParser extends \Illuminate\Support\Facades\Facade
	{
		/**
		 * @return string
		 */
		protected static function getFacadeAccessor() : string
		{
			return 'url-parser';
		}
	}
}