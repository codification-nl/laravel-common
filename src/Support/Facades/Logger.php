<?php

namespace Codification\Common\Support\Facades
{
	/**
	 * @method static void useUserModel(string|\Illuminate\Database\Eloquent\Model $model)
	 * @method static void useEntryModel(string|\Codification\Common\Log\LogEntry $model)
	 * @method static void enable()
	 *
	 * @see          \Codification\Common\Log\Logger
	 */
	final class Logger extends \Illuminate\Support\Facades\Facade
	{
		/**
		 * @return string
		 */
		protected static function getFacadeAccessor() : string
		{
			return 'logger';
		}
	}
}