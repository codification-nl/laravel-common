<?php

namespace Codification\Common\Support\Facades
{
	/**
	 * @method static void from(null|string $address)
	 * @method static void cache(null|\Closure $fn)
	 * @method static void logger(null|\Closure $fn)
	 * @method static bool isValid(null|string $address, string $from = null, int $timeout = \Codification\Common\Mail\Email::SMTP_TIMEOUT)
	 * @method static string|null sanitize(null|string $address)
	 *
	 * @see          \Codification\Common\Mail\Email
	 */
	final class Email extends \Illuminate\Support\Facades\Facade
	{
		/**
		 * @return string
		 */
		protected static function getFacadeAccessor() : string
		{
			return 'email';
		}
	}
}