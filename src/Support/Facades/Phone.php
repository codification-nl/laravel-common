<?php

namespace Codification\Common\Support\Facades
{
	/**
	 * @method static \Codification\Common\Phone\Phone|null make(string|null $number, string $region_code, \Codification\Common\Phone\ParseErrorType|null $out_parse_error = null)
	 * @method static bool validate(string|null $number, string $region_code, \Codification\Common\Phone\PhoneType|int|string $type = \Codification\Common\Phone\PhoneType::BOTH, \Codification\Common\Phone\ParseErrorType|null $out_parse_error = null)
	 * @method static string|null getRegionCode(string|null $number, string|null $region_code = null)
	 *
	 * @see \Codification\Common\Phone\Phone
	 */
	final class Phone extends \Illuminate\Support\Facades\Facade
	{
		/**
		 * @return string
		 */
		protected static function getFacadeAccessor() : string
		{
			return 'phone';
		}
	}
}