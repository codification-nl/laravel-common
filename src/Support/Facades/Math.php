<?php

namespace Codification\Common\Support\Facades
{
	/**
	 * @method static \Codification\Common\Math\Number add(\Codification\Common\Math\Number|string|float|int $lhs, \Codification\Common\Math\Number|string|float|int $rhs)
	 * @method static \Codification\Common\Math\Number sub(\Codification\Common\Math\Number|string|float|int $lhs, \Codification\Common\Math\Number|string|float|int $rhs)
	 * @method static \Codification\Common\Math\Number mul(\Codification\Common\Math\Number|string|float|int $lhs, \Codification\Common\Math\Number|string|float|int $rhs)
	 * @method static \Codification\Common\Math\Number div(\Codification\Common\Math\Number|string|float|int $lhs, \Codification\Common\Math\Number|string|float|int $rhs)
	 * @method static \Codification\Common\Math\Number mod(\Codification\Common\Math\Number|string|float|int $lhs, \Codification\Common\Math\Number|string|float|int $rhs)
	 *
	 * @see \Codification\Common\Math\Math
	 */
	final class Math extends \Illuminate\Support\Facades\Facade
	{
		/**
		 * @return string
		 */
		protected static function getFacadeAccessor() : string
		{
			return 'math';
		}
	}
}