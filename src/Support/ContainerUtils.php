<?php

namespace Codification\Common\Support
{
	use Illuminate;
	use Codification\Common\Country;

	final class ContainerUtils
	{
		/**
		 * @return \Illuminate\Foundation\Application
		 */
		public static function app() : \Illuminate\Foundation\Application
		{
			/** @var \Illuminate\Foundation\Application $app */
			$app = Illuminate\Container\Container::getInstance();

			return $app;
		}

		/**
		 * @template     T
		 *
		 * @param string $abstract
		 *
		 * @psalm-return T
		 * @return mixed
		 *
		 * @throws \Codification\Common\Support\Exceptions\ResolutionException
		 */
		public static function resolve(string $abstract)
		{
			try
			{
				/** @psalm-var T $container */
				$container = Illuminate\Container\Container::getInstance()->make($abstract);
			}
			catch (Illuminate\Contracts\Container\BindingResolutionException $e)
			{
				throw new Exceptions\ResolutionException($abstract, $e->getPrevious());
			}

			return $container;
		}

		/**
		 * @param string|null $locale
		 * @psalm-param 0|1   $case = CASE_LOWER
		 * @param int         $case = CASE_LOWER
		 *
		 * @return string
		 *
		 * @throws \Codification\Common\Country\Exceptions\CountryCodeException
		 */
		public static function resolveLocale(string $locale = null, int $case = \CASE_LOWER) : string
		{
			$locale = \sanitize($locale);

			if ($locale === null)
			{
				$locale = self::app()->getLocale();
			}

			Country\Country::ensureValid($locale);

			if ($case === \CASE_UPPER)
			{
				return \strtoupper($locale);
			}

			return \strtolower($locale);
		}
	}
}