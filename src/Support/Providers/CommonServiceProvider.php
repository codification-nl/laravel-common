<?php

namespace Codification\Common\Support\Providers
{
	use Illuminate;
	use Codification\Common\Validation;
	use Codification\Common\Support;

	class CommonServiceProvider extends Illuminate\Support\ServiceProvider
	{
		/**
		 * @var array<string, string>
		 * @psalm-var array<string, class-string<\Codification\Common\Support\Contracts\Bindable>>
		 */
		public static /*@array*/
			$facades = [
			'country'    => \Codification\Common\Country\Country::class,
			'logger'     => \Codification\Common\Log\Logger::class,
			'email'      => \Codification\Common\Mail\Email::class,
			'math'       => \Codification\Common\Math\Math::class,
			'money'      => \Codification\Common\Money\Money::class,
			'phone'      => \Codification\Common\Phone\Phone::class,
			'url-parser' => \Codification\Common\Url\UrlParser::class,
		];

		/**
		 * @return void
		 */
		public function register() : void
		{
			foreach (self::$facades as $abstract => $class)
			{
				/*@$this->app->bind($abstract, static fn() => new $class());*/
				$this->app->bind($abstract, static function () use ($class)
					{
						return new $class();
					});
			}
		}

		/**
		 * @return void
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		public function boot() : void
		{
			Validation\Validator::extend();

			self::mixinCollectionUtils();
		}

		/**
		 * @return void
		 * @throws \Codification\Common\Support\Exceptions\ShouldNotHappenException
		 */
		private static function mixinCollectionUtils() : void
		{
			try
			{
				Illuminate\Support\Collection::mixin(new Support\CollectionUtils());
			}
			catch (\ReflectionException $e)
			{
				throw new Support\Exceptions\ShouldNotHappenException('Failed to mixin [CollectionUtils]', $e);
			}
		}
	}
}